
<% Response.ContentType="text/plain" %>    
<!-- #include file="Config.asp" -->
<% 
    dim cookielg: cookielg = Request.QueryString("lg") 
        dim cookieposition: cookieposition = Request.QueryString("pos") 
        dim format_curency  : format_curency  =  Request.QueryString("fc") 
        if format_curency & "" = ""  then
            format_curency = 1
        end if
        i_curencyformat = format_curency
        if cookielg & "" = "" then
        cookielg = ""
        cookieposition = 1
        end if               
Function URLDecode(ByVal What)
'URL decode Function
'2001 Antonin Foller, PSTRUH Software, http://www.motobit.com
  Dim Pos, pPos

  'replace + To Space
  What = Replace(What, "+", " ")

  on error resume Next
  Dim Stream: Set Stream = CreateObject("ADODB.Stream")
  If err = 0 Then 'URLDecode using ADODB.Stream, If possible
    on error goto 0
    Stream.Type = 2 'String
    Stream.Open

    'replace all %XX To character
    Pos = InStr(1, What, "%")
    pPos = 1
    Do While Pos > 0
      Stream.WriteText Mid(What, pPos, Pos - pPos) + _
        Chr(CLng("&H" & Mid(What, Pos + 1, 2)))
      pPos = Pos + 3
      Pos = InStr(pPos, What, "%")
    Loop
    Stream.WriteText Mid(What, pPos)

    'Read the text stream
    Stream.Position = 0
    URLDecode = Stream.ReadText

    'Free resources
    Stream.Close
  Else 'URL decode using string concentation
    on error goto 0
    'UfUf, this is a little slow method. 
    'Do Not use it For data length over 100k
    Pos = InStr(1, What, "%")
    Do While Pos>0 
      What = Left(What, Pos-1) + _
        Chr(Clng("&H" & Mid(What, Pos+1, 2))) + _
        Mid(What, Pos+3)
      Pos = InStr(Pos+1, What, "%")
    Loop
    URLDecode = What
  End If
End Function
    
 
    Function GetDataLanguage(Byval slanguage)
        dim objRdsLanguage
        dim SQL_Language
        Set objConconfig = Server.CreateObject("ADODB.Connection")
        Set objRdsLanguage = Server.CreateObject("ADODB.Recordset") 
        Dim strArr, arrValue  

        objConconfig.Open sConnString
        SQL_Language = "select top 1 l.*  FROM Language_LabelSettings l  WITH(NOLOCK) where l.language_code='"+ slanguage +"' order by l.id asc "
        objRdsLanguage.Open SQL_Language, objConconfig

        While NOT objRdsLanguage.Eof 
            For each item in objRdsLanguage.Fields
                if strArr & "" <> "" then
                    strArr = strArr & "~" + item.Name & "|" & objRdsLanguage(item.Name)
                else
                    strArr = item.Name & "|" & objRdsLanguage(item.Name)
                end if
             Next             
            objRdsLanguage.movenext()
        wend

        objRdsLanguage.Close()    
        objConconfig.Close()
        Set objRdsLanguage = nothing
        Set objConconfig = nothing

        if strArr & "" <> "" then
            arrValue = Split(strArr,"~")
        end if
        GetDataLanguage = arrValue
    End Function

    Function FilterData(Byval strArray, Byval lable)
        dim dataReturn: dataReturn = ""
		if IsArray(strArray) then
			dim arrFilter: arrFilter = Filter(strArray,lable)        
			For indexArr = 0 to ubound(arrFilter)
				dim fieldName
				fieldName = Split(arrFilter(indexArr),"|")	
				if IsArray(fieldName) And UBound(fieldName) >= 0 then
					if fieldName(0) = lable then
						dataReturn = fieldName(1)
					end if
				end if
			Next
		end if
        FilterData = dataReturn
    End Function
      Dim  arrData : arrData = GetDataLanguage(cookielg)
   ' task 263
    Dim StrAllergen : StrAllergen = ""
    function FindAllergen(byval allergen )
             allergens  = "," & replace(allergens," ","") & ","   
            dim result : result = ""
            dim allergenID : allergenID = ""
        if StrAllergen <> "" then
           dim arrAllergen : arrAllergen =  split(StrAllergen,"[**]")
               dim index :  index = 0
            for index = 0 to ubound(arrAllergen) 
                if arrAllergen(index) & "" <> "" then
                    allergenID = split(arrAllergen(index),"|")(0)
                    if allergen = allergenID  then
                        result = arrAllergen(index)
                    end if 
                end if
            next
        end if
        FindAllergen = result
    end function
    'task 263
    Set objCon = Server.CreateObject("ADODB.Connection")
    Set objRds = Server.CreateObject("ADODB.Recordset") 
 
    Dim vRestaurantId
    vRestaurantId = Request.QueryString("id_r")
    objCon.Open sConnString
    Dim SQL_B 
    SQL_B  ="Select Id,EnableAllergen,EnableSuitableFor,s_BannerURL,Name,PostalCode "
    SQL_B =  SQL_B & " ,menupagetext,orderonlywhenopen,disablelaterdelivery,individualpostcodeschecking "
    SQL_B =  SQL_B & " ,individualpostcodes,individualpostcodes,DeliveryMaxDistance "
    SQL_B =  SQL_B & ",DeliveryFreeDistance,DeliveryMinAmount,DeliveryFee"
    SQL_B =  SQL_B & " ,AverageDeliveryTime,AverageCollectionTime,DeliveryChargeOverrideByOrderValue "
    SQL_B =  SQL_B & " ,Latitude,Longitude,distancecalmethod,Name,css,ImgUrl,announcement "
    SQL_B =  SQL_B & ",Telephone,Email,Address,FoodType,CURRENCYSYMBOL,enable_stockstatus,i_showformatcurrency "
    SQL_B =  SQL_B & " FROM BusinessDetails with(nolock)  WHERE Id = " & vRestaurantId
     objRds.Open SQL_B , objCon
     dim objRdsMainCategory
    Set objRdsMainCategory = Server.CreateObject("ADODB.Recordset") 
  Dim limittopping , s_BannerURL ,enable_stockstatus,i_showformatcurrency
 ' check url
     '' Get Url Menu, checkout , thanks
    dim MenuURL,CheckoutURL,ThankURL
        CheckoutURL = SITE_URL& "checkOut.asp?id_r=" & vRestaurantId
    
   
      
'check opening times
    i_showformatcurrency = objRds("enable_stockstatus") & "" 
     if i_showformatcurrency = "" then
        i_showformatcurrency = "1"
    end if
     i_showcurrency = i_showformatcurrency
    ' task 263 
    enable_stockstatus =   objRds("enable_stockstatus") & "" 
    CURRENCYSYMBOL = objRds("CURRENCYSYMBOL") & ""
    EnableAllergen = objRds("EnableAllergen") & ""
    EnableSuitableFor = objRds("EnableSuitableFor") & ""
    if EnableAllergen = "" then
        EnableAllergen = "No"
    end if 
    if EnableSuitableFor = "" then
        EnableSuitableFor = "No"
    end if 

    Dim SQL1 : SQL1 = "select ID,icon,Type, "
    if cookieposition = 1 then
        SQL1 = SQL1 & "Name from allergen with(nolock) "
    else
        SQL1 = SQL1 & "Name"& cookieposition &" AS Name from allergen with(nolock) where IdBusinessDetail =  " & vRestaurantId
    end if
    if EnableAllergen = "Yes" and EnableSuitableFor = "No"  then
        SQL1 = SQL1 &  " and type ='Allergen' " 
    elseif EnableAllergen = "No" and EnableSuitableFor = "Yes"  then
        SQL1 = SQL1 &  " and type ='SuitableFor' " 
    elseif EnableAllergen = "No" and EnableSuitableFor = "No"  then
         SQL1 = SQL1 &  " and 1 != 1 " 
    end if
    
  SQL1 = SQL1 &  "  order by  Name "
    Dim Rs_Allergen : set Rs_Allergen =  Server.CreateObject("ADODB.Recordset")
        Rs_Allergen.open  SQL1 ,objCon
    while not Rs_Allergen.EOF 
        if StrAllergen = "" then
            StrAllergen =   Rs_Allergen("ID") & "|" & Rs_Allergen("Name") & "|" & Rs_Allergen("icon") & "|" & Rs_Allergen("Type")
        else
            StrAllergen =  StrAllergen & "[**]" & Rs_Allergen("ID") & "|" & Rs_Allergen("Name") & "|" & Rs_Allergen("icon") & "|" & Rs_Allergen("Type")
        end if
     
        Rs_Allergen.movenext() 
    wend
        Rs_Allergen.close()
    set Rs_Allergen = nothing
    'End task 263

    s_BannerURL = trim( objRds("s_BannerURL") & "" ) 
    


%>
<%
              
                      Dim   SQLCategory 
                            SQLCategory ="  SELECT DISTINCT   mc.id, displayorder   "
                            if cookieposition = 1 then
                                SQLCategory = SQLCategory & ",mc.NAME, mc.description "                        
                            else
                                SQLCategory = SQLCategory & ",mc.Name"& cookieposition &" as NAME, mc.description"& cookieposition &" as description"
                            end if
                            SQLCategory = SQLCategory & " FROM   menucategories   mc  with(nolock)      "
                            SQLCategory = SQLCategory & " ORDER  BY mc.displayorder; "
                            objRdsMainCategory.Open   SQLCategory , objCon
                         
                
                    Dim vCategoryId                
                    Dim vMenuItemId
                    Dim vMenuItemPrice
                    Dim f  
                   
                    dim categoryID,CategoryName,CategoryDescription
                    dim s_ContainAllergen_p,s_MayContainAllergen_p,s_SuitableFor_p 
                            dim s_ContainAllergen_m,s_MayContainAllergen_m,s_SuitableFor_m
                    while not objRdsMainCategory.EOF
                                    
                            categoryID = objRdsMainCategory("ID")
                            CategoryName = objRdsMainCategory("Name")
                            CategoryDescription = objRdsMainCategory("Description")
                            %>
<div class="categroup-<%=categoryID %> "></div>
<div id="group-categroup-<%=categoryID %>" class="product-line-heading clearfix" onclick="ShowdishpropertiesV2('categroup-<%=categoryID %>')">
<h4 class="product-line-heading__cat pull-left" >
<a id="menucat_<%=categoryID %>" name="menucat_<%=categoryID %>" ></a>
<%= CategoryName%>   
</h4>
<div class="product-line-heading__icon-wrapper is-vertical-center">
<img class="product-line-heading__icon" src="<%=SITE_URL %>images/menu-category-collapse--retina.png" alt="" id="imgcategroup-<%=categoryID %>">
</div>            
<% if CategoryDescription & "" <> "" then %>
<div class="product-line-heading__cat-des">
<%= CategoryDescription %>
</div>    
<% end if %>  
</div>
<div id="categroup-<%=categoryID %>" class="group-ptoduct-line" data-type="group-cate">
<%
dim objRds_MenuItem : set objRds_MenuItem  =  Server.CreateObject("ADODB.Recordset")
dim SQL 
    SQL = " SELECT mi.*,  "
    if cookieposition = 1 then
        SQL = SQL & "mi.Name AS Name, mip.Name AS PropertyName,"
    else
        SQL = SQL & "mi.Name"& cookieposition &" AS Name, mip.Name"& cookieposition &" AS PropertyName,"
    end if
    SQL =SQL & " mip.Id AS PropertyId,  "
    SQL =SQL & "mip.Price AS PropertyPrice,  mi.allowtoppings AS miallowtoppings, "
    SQL =SQL & " mi.ToppingGroupIDs AS ToppingGroupIDs,mip.ToppingGroupIDs AS MToppingGroupIDs, "
    SQL =SQL & " mip.allowtoppings AS mipallowtoppings,mip.i_displaysort  "
    SQL = SQL & ",mip.s_ContainAllergen as s_ContainAllergen_p,mip.s_MayContainAllergen as s_MayContainAllergen_p,mip.s_SuitableFor as s_SuitableFor_p "
    SQL = SQL & ",mi.s_ContainAllergen as s_ContainAllergen_m,mi.s_MayContainAllergen as s_MayContainAllergen_m,mi.s_SuitableFor as s_SuitableFor_m "
    SQL =SQL & " FROM  MenuItems AS mi "
    SQL =SQL & " LEFT JOIN MenuItemProperties AS mip ON mi.Id = mip.IdMenuItem "
    SQL =SQL & "WHERE    mi.idbusinessdetail =  " & vRestaurantId & "  AND mi.hidedish<>1 "
    if enable_stockstatus & "" = "1" then
        SQL = SQL & "  AND isnull(mi.i_quantity,0) > 0 "
    end if 
    SQL =SQL & " ORDER BY mi.i_displaysort,mi.id,mip.i_displaysort,mip.Id; "

objRds_MenuItem.Open SQL, objCon 

' Load Menu Item 
objRds_MenuItem.Filter =  " IdMenuCategory = " & categoryID  & ""

dim Code,MenuDescription,dishpropertygroupid,hidedish
dim MenuItemName,Photo,MenuPrice,menuPrintingName,Spicyness,Vegetarian
dim PropertyName,PropertyId,PropertyPrice,miallowtoppings,mipallowtoppings,ToppingGroupIDs,MToppingGroupIDs
                            
MenuItemName = ""
dim menuItemNameID : menuItemNameID = ""
while not objRds_MenuItem.EOF
s_ContainAllergen_m = replace(objRds_MenuItem("s_ContainAllergen_m") & ""," ","")
s_MayContainAllergen_m = replace(objRds_MenuItem("s_MayContainAllergen_m") & ""," ","")
s_SuitableFor_m = replace(objRds_MenuItem("s_SuitableFor_m") & ""," ","")
                            
s_ContainAllergen_p = replace(objRds_MenuItem("s_ContainAllergen_p") & ""," ","")
s_MayContainAllergen_p = replace(objRds_MenuItem("s_MayContainAllergen_p") & ""," ","")
s_SuitableFor_p = replace(objRds_MenuItem("s_SuitableFor_p") & ""," ","") 
vMenuItemId = objRds_MenuItem("Id")
Code =  objRds_MenuItem("Code")
MenuDescription = objRds_MenuItem("Description")
dishpropertygroupid = objRds_MenuItem("dishpropertygroupid")
hidedish = objRds_MenuItem("hidedish")
MenuItemName = objRds_MenuItem("Name")
Photo = objRds_MenuItem("Photo")
MenuPrice = objRds_MenuItem("Price")
menuPrintingName = objRds_MenuItem("PrintingName")
Spicyness = objRds_MenuItem("Spicyness")
'Vegetarian = objRds_MenuItem("Vegetarian")
PropertyName = objRds_MenuItem("PropertyName")
PropertyId = "-1"
If Not IsNull(objRds_MenuItem("PropertyId")) Then
PropertyId = objRds_MenuItem("PropertyId")
PropertyPrice = objRds_MenuItem("PropertyPrice")   
if MenuPrice & "" = "0" or MenuPrice & "" = ""  then 
MenuPrice = PropertyPrice
end if                     
End If
                                   
miallowtoppings = objRds_MenuItem("miallowtoppings")
mipallowtoppings = objRds_MenuItem("mipallowtoppings")
ToppingGroupIDs = objRds_MenuItem("ToppingGroupIDs")
MToppingGroupIDs = objRds_MenuItem("MToppingGroupIDs")

dim class_noborder : class_noborder = ""
if menuItemNameID = vMenuItemId then
class_noborder = " no-border"
End if
dim parent : parent = "" 
if menuItemNameID <> vMenuItemId then
parent = "parent='0'"
end if
%>
<div class="product-line <%=class_noborder %>" name="<%=vMenuItemId %>" <%=parent %> >
<% 
if menuItemNameID <> vMenuItemId then  %>
<div class="product-line__content-left<%=class_noborder %>">
<div class="d-flex-center d-flex-start">
<%
dim styleMarginleft : styleMarginleft =""
If Photo <> "" Then 
styleMarginleft = "style='margin-left:12px;' "
photo=1%>
<div  class="product10w photo" data-toggle="modal" data-target="#lightbox">  
<img data-src="<%=SITE_URL %>Images/<%=vRestaurantId %>/<%= objRds_MenuItem("Photo")%>" class="img-rounded lazy" alt="<%= MenuItemName%>" style="display-block" /> 
<div class="overlay">
<a href="javascript:;"  class="magnifying-glass-icon foobox" style="top:12px;left:20px;">
<i class="fa fa-search"></i>
</a>
</div>
</div>	
<%End If %>
<div class="product-line__number" <%=styleMarginleft %>>
<% If Code <> "" Then 
code=1%><%= objRds_MenuItem("Code")  %>.
<%End If %>
</div>
<div class="product-line__description desc "   s_ContainAllergen_m="<%=s_ContainAllergen_m & "|" & s_MayContainAllergen_m %>" s_MayContainAllergen_m="<%=s_MayContainAllergen_m %>" s_SuitableFor_m="<%=s_SuitableFor_m %>">
<%=MenuItemName %>
<%
dim index_m ,s_contain
                                           
if s_ContainAllergen_m & "" <> "" then 
''FindAllergen
dim   arr_s_ContainAllergen_m : arr_s_ContainAllergen_m = split(s_ContainAllergen_m,",")
index_m = 0
for index_m = 0 to ubound(arr_s_ContainAllergen_m)
s_contain = FindAllergen(arr_s_ContainAllergen_m(index_m) )
if s_contain & "" <> "" then
%>
<img width="17" height="17" data-container="body" data-toggle="tooltip"  src="<%=SITE_URL %>Images/allergen/png/<%=replace( split(s_contain,"|")(2),"amber","red")   %>" title="<%=FilterData(arrData,"scontains") %> <%=split(s_contain,"|")(1) %>"  alt="<%=FilterData(arrData,"scontains") %> <%=split(s_contain,"|")(1) %>" />
<%
end if
next
%>
<% end if %>
<% if s_MayContainAllergen_m & "" <> "" then 
''FindAllergen
dim arr_s_MayContainAllergen_m : arr_s_MayContainAllergen_m= split(s_MayContainAllergen_m,",")
index_m = 0
for index_m = 0 to ubound(arr_s_MayContainAllergen_m)
s_contain =FindAllergen(arr_s_MayContainAllergen_m(index_m) )
if s_contain & "" <> "" then%>
<img width="17" height="17" data-container="body" data-toggle="tooltip"  src="<%=SITE_URL %>Images/allergen/png/<%=split(s_contain,"|")(2)  %>" title="<%=FilterData(arrData,"may_contain") %> <%=split(s_contain,"|")(1) %>" alt="<%=FilterData(arrData,"may_contain") %> <%=split(s_contain,"|")(1) %>" />
<%
end if
next
%>
<% end if %>
<% if s_SuitableFor_m & "" <> "" then 
''FindAllergen
dim arr_s_SuitableFor_m : arr_s_SuitableFor_m= split(s_SuitableFor_m,",")
index_m = 0
for index_m = 0 to ubound(arr_s_SuitableFor_m)
s_contain =FindAllergen( arr_s_SuitableFor_m(index_m) )
if s_contain & "" <> "" then%>
<img width="17" height="17" data-container="body" data-toggle="tooltip"  src="<%=SITE_URL %>Images/allergen/png/<%=split(s_contain,"|")(2)  %>"  title="<%=split(s_contain,"|")(1) %>" alt="<%=split(s_contain,"|")(1) %>"  />
<%
end if
next
%>
<% end if %>
<%
dim spicytitle  : spicytitle = FilterData(arrData,"mildly_spicy")'"Mildly Spicy" 
                                                
if Spicyness = 2  then
    spicytitle = FilterData(arrData,"spicy")'"Spicy" 
elseif Spicyness = 3  then
    spicytitle = FilterData(arrData,"very_spicy") '"Very Spicy" 
end if
If Spicyness> 0 Then %>
<img src="<%=SITE_URL %>Images/spicy_<%= Spicyness %>.png?v=1.1"  height="17" alt="<%=spicytitle %>" title="<%=spicytitle %>"  data-container="body" data-toggle="tooltip"  />
<%End If %><br />
<% if MenuDescription & "" <> "" then %>
<i><span class="small"><%= MenuDescription %></span></i>
<% end if %>
</div>
</div>
</div>
<% end if
menuItemNameID = vMenuItemId
%>                                     
<div class="product-line__content-right " style="width:85%">
<div class="d-flex-center d-flex-end">
<div class="product-line__property-name"  s_ContainAllergen_p="<%=s_ContainAllergen_p & "|" & s_MayContainAllergen_p %>" s_MayContainAllergen_p="<%=s_MayContainAllergen_p %>"  s_SuitableFor_p="<%=s_SuitableFor_p %>"><%=PropertyName %></div> 
<%
if s_ContainAllergen_p & "" <> "" then 
''FindAllergen
dim   arr_s_ContainAllergen_p : arr_s_ContainAllergen_p = split(s_ContainAllergen_p,",")
index_m = 0
for index_m = 0 to ubound(arr_s_ContainAllergen_p)
s_contain = FindAllergen(arr_s_ContainAllergen_p(index_m) )
if s_contain & "" <> "" then
%>
 <img width="17" height="17" data-toggle="tooltip"  src="<%=SITE_URL %>Images/allergen/png/<%=replace(split(s_contain,"|")(2),"amber","red")  %>" title="<%=FilterData(arrData,"scontains") %> <%=split(s_contain,"|")(1) %>"  alt="<%=FilterData(arrData,"scontains") %> <%=split(s_contain,"|")(1) %>" />
<%
end if
next
%>
<% end if %>
<% if s_MayContainAllergen_p & "" <> "" then 
''FindAllergen
dim arr_s_MayContainAllergen_p : arr_s_MayContainAllergen_p= split(s_MayContainAllergen_p,",")
index_m = 0
for index_m = 0 to ubound(arr_s_MayContainAllergen_p)
s_contain =FindAllergen(arr_s_MayContainAllergen_p(index_m) )
if s_contain & "" <> "" then%>
  <img width="17" height="17" data-toggle="tooltip"  src="<%=SITE_URL %>Images/allergen/png/<%=split(s_contain,"|")(2)  %>" title="<%=FilterData(arrData,"may_contain") %> <%=split(s_contain,"|")(1) %>"  alt="<%=FilterData(arrData,"may_contain") %> <%=split(s_contain,"|")(1) %>" />
<%
end if
next
%>
<% end if %>
<% if s_SuitableFor_p & "" <> "" then 
''FindAllergen
dim arr_s_SuitableFor_p : arr_s_SuitableFor_p= split(s_SuitableFor_p,",")
index_m = 0
for index_m = 0 to ubound(arr_s_SuitableFor_p)
s_contain =FindAllergen( arr_s_SuitableFor_p(index_m) )
if s_contain & "" <> "" then%>
<img width="17" height="17" data-toggle="tooltip"  src="<%=SITE_URL %>Images/allergen/png/<%=split(s_contain,"|")(2)  %>" title="<%=split(s_contain,"|")(1) %>" alt="<%=split(s_contain,"|")(1) %>"  />
<%
end if
next
%>
<% end if %>
<% donotshowprice="n"
dishpropertiestext=""
pricefrom=0
' code to check if other dish properties are applicable to this product
if dishpropertygroupid & "" <>"" then%>
<%
'Set objCon_properties = Server.CreateObject("ADODB.Connection")
Set objRds_properties = Server.CreateObject("ADODB.Recordset") 
          
'objCon_properties.Open sConnString
 if cookieposition = 1 then
    SQL = "SELECT id, dishpropertyrequired, dishpropertypricetype, dishpropertygroup FROM MenuDishpropertiesGroups where id in (" & dishpropertygroupid & ") order by i_displaysort,id "
else
    SQL = "SELECT id, dishpropertyrequired, dishpropertypricetype, dishpropertygroup"& cookieposition &" AS dishpropertygroup FROM MenuDishpropertiesGroups where id in (" & dishpropertygroupid & ") order by i_displaysort,id"
end if

objRds_properties.Open SQL , objCon

While NOT objRds_properties.Eof 
dishpropertiestext =  dishpropertiestext & "<div class=""dishproperties__title"">" & objRds_properties("dishpropertygroup") & " </div> "
dishpropertiestext =  dishpropertiestext & " <select name=""" & objRds_properties("id") & """ id=""" & objRds_properties("id") & """ class=""form-control selectpicker"" data-group=""dishproperties" & vMenuItemId & "-" & PropertyId & """"
if objRds_properties("dishpropertyrequired")<>1  then
dishpropertiestext = dishpropertiestext & " data-required=""n"">"
dishpropertiestext = dishpropertiestext & "><option value=""0"">-- "&FilterData(arrData,"sselect")&" --</option>"
else
dishpropertiestext = dishpropertiestext & " data-required=""y"" data-caption=""Please choose " & replace(objRds_properties("dishpropertygroup")&"","""","") & """>"
dishpropertiestext = dishpropertiestext & "><option value=""0"">-- " & FilterData(arrData,"sselect") & " --</option>"
end if
						
'Set objCon_propertiesitems = Server.CreateObject("ADODB.Connection")
Set objRds_propertiesitems = Server.CreateObject("ADODB.Recordset") 

SQL = "SELECT dishproperty,dishpropertyprice,id,i_displaysort,s_ContainAllergen,s_MayContainAllergen,s_SuitableFor FROM MenuDishproperties with(nolock)    where dishpropertygroupid=" & objRds_properties("id")  & " order by i_displaysort, id "
if cookieposition = 1 then
    SQL = "SELECT dishproperty,dishpropertyprice,id,i_displaysort,s_ContainAllergen,s_MayContainAllergen,s_SuitableFor FROM MenuDishproperties with(nolock)    where dishpropertygroupid=" & objRds_properties("id")  & " order by i_displaysort, id "
else
    SQL = "SELECT isnull(dishproperty"& cookieposition &",'') as dishproperty ,dishpropertyprice,id,i_displaysort,s_ContainAllergen,s_MayContainAllergen,s_SuitableFor FROM MenuDishproperties with(nolock)    where dishpropertygroupid=" & objRds_properties("id")  & " order by i_displaysort, id "
end if

objRds_propertiesitems.Open SQL, objCon
dim s_ContainAllergen_dp,s_MayContainAllergen_dp,s_SuitableFor_dp  
dim htmltooltip : htmltooltip = "" 
While NOT objRds_propertiesitems.Eof 
dim htmlicon : htmlicon = ""
'   htmltooltip = ""
s_ContainAllergen_dp = replace(objRds_propertiesitems("s_ContainAllergen") & ""," ","") 
s_MayContainAllergen_dp = replace(objRds_propertiesitems("s_MayContainAllergen") & "" ," ","") 
s_SuitableFor_dp  = replace(objRds_propertiesitems("s_SuitableFor") & ""," ","") 
htmltooltip = htmltooltip & "<b>" &  objRds_propertiesitems("dishproperty") & "</b>"  & "<br/>"
dim htmltooltip1 : htmltooltip1 = ""
Dim isAllergen : isAllergen = false
if s_ContainAllergen_dp & "" <> "" then                                                   
dim   arr_s_ContainAllergen_dp : arr_s_ContainAllergen_dp = split(s_ContainAllergen_dp,",")
index_m = 0
for index_m = 0 to ubound(arr_s_ContainAllergen_dp)
s_contain = FindAllergen(arr_s_ContainAllergen_dp(index_m) )
if s_contain & "" <> "" then
if instr(htmltooltip1,"Contains") = 0 then
htmltooltip1 = htmltooltip1 &  FilterData(arrData,"scontains") '"Contains: " 
end if
htmltooltip1 =  htmltooltip1 & " <img width=""17"" height=""17""  src=""" & SITE_URL & "Images/allergen/png/" & replace( split(s_contain,"|")(2),"amber","red") & """ /> "
htmltooltip1 =  htmltooltip1 &  split(s_contain,"|")(1) & ", "
htmlicon = htmlicon &  SITE_URL &  "Images/allergen/png/"  & replace(split(s_contain,"|")(2),"amber","red")  & ";"                                                                
end if
next
end if
if htmltooltip1 & "" <> "" then
isAllergen = true
htmltooltip1 =  left(trim(htmltooltip1),len(trim(htmltooltip1))-1)
htmltooltip = htmltooltip & "<span class=""tip-allergen"">" &  htmltooltip1 & "</span><br/>"
end if
htmltooltip1  =""
if s_MayContainAllergen_dp & "" <> "" then                                                   
dim   arr_s_MayContainAllergen_dp : arr_s_MayContainAllergen_dp = split(s_MayContainAllergen_dp,",")
index_m = 0
for index_m = 0 to ubound(arr_s_MayContainAllergen_dp)
s_contain = FindAllergen(arr_s_MayContainAllergen_dp(index_m) )
if s_contain & "" <> "" then
if instr(htmltooltip1,"May Contain") = 0 then
htmltooltip1 = htmltooltip1 &  FilterData(arrData,"may_contain") '"May Contain: " 
end if
htmltooltip1 =  htmltooltip1 & " <img width=""17"" height=""17""  src=""" & SITE_URL & "Images/allergen/png/" & split(s_contain,"|")(2) & """ /> "
htmltooltip1 =  htmltooltip1 & split(s_contain,"|")(1) & ", "
htmlicon = htmlicon &  SITE_URL &  "Images/allergen/png/"  & split(s_contain,"|")(2)  & ";"   
end if
next
end if
if htmltooltip1 & "" <> "" then
isAllergen = true
htmltooltip1 =  left(trim(htmltooltip1),len(trim(htmltooltip1))-1)
htmltooltip = htmltooltip & "<span class=""tip-allergen"">" &  htmltooltip1 & "</span><br/>"
end if
htmltooltip1  =""
if s_SuitableFor_dp & "" <> "" then                                                   
dim   arr_s_SuitableFor_dp : arr_s_SuitableFor_dp = split(s_SuitableFor_dp,",")
index_m = 0
for index_m = 0 to ubound(arr_s_SuitableFor_dp)
s_contain = FindAllergen(arr_s_SuitableFor_dp(index_m) )
 if s_contain & "" <> "" then
if instr(htmltooltip1,"Suitable For") = 0 then
htmltooltip1 = htmltooltip1 &  FilterData(arrData,"suitable_for") '"Suitable For: " 
end if
htmltooltip1 =  htmltooltip1 & " <img width=""17"" height=""17""  src=""" & SITE_URL & "Images/allergen/png/" & split(s_contain,"|")(2) & """ /> "
htmltooltip1 =  htmltooltip1 & split(s_contain,"|")(1) & ", "
htmlicon = htmlicon &  SITE_URL &  "Images/allergen/png/"  & split(s_contain,"|")(2)  & ";"   
end if
next
end if
if htmltooltip1 & "" <> "" then
isAllergen = true
htmltooltip1 =  left(trim(htmltooltip1),len(trim(htmltooltip1))-1)
htmltooltip = htmltooltip & "<span class=""tip-allergen"">" &  htmltooltip1 & "</span><br/>"
end if
if isAllergen =false  then
 htmltooltip = htmltooltip & "<span class=""tip-allergen"">" & FilterData(arrData,"no_allergens") &"</span><br/>"
end if   
'  htmltooltip =  htmltooltip & "<br/>"

add=""
if objRds_properties("dishpropertypricetype")="add" then
add=" - + "
else
donotshowprice="y"
if pricefrom & "" = "0" or pricefrom & "" = ""  then
pricefrom=objRds_propertiesitems("dishpropertyprice")
end if
end if
if add = "" then
add = " - "    
end if 
' dishpropertiestext = dishpropertiestext & "<option value=""" & objRds_propertiesitems("id") & """>" & objRds_propertiesitems("dishproperty") & add & " " &  CURRENCYSYMBOL & FormatNumber(objRds_propertiesitems("dishpropertyprice"),2) & "</option>"
dishpropertiestext = dishpropertiestext & "<option data-thumbnail=""" & htmlicon & """ s_SuitableFor_dp="""& s_SuitableFor_dp &"""  s_MayContainAllergen_dp="""& s_MayContainAllergen_dp  &""" s_ContainAllergen_dp="""& s_ContainAllergen_dp & "|" & s_MayContainAllergen_dp &""" value=""" & objRds_propertiesitems("id") & """>" & objRds_propertiesitems("dishproperty") & add & " " &   formatcurentcyC( FormatNumber(objRds_propertiesitems("dishpropertyprice"),2)) & "</option>"
objRds_propertiesitems.MoveNext
wend 
objRds_propertiesitems.close()
set objRds_propertiesitems = nothing        
dishpropertiestext = dishpropertiestext & "</select>"
'' Add Add tooltip here
if instr(htmltooltip,"Contains:") =  0 and instr(htmltooltip,"May Contain:") =  0 and  instr(htmltooltip,"Suitable For:") =  0 then
htmltooltip = ""
else
dishpropertiestext =  dishpropertiestext & "   <span class=""glyphicon glyphicon-exclamation-sign append text-info tip"" data-tip=""tip-"&objRds_properties("id")&""" ></span> <br>"    
dishpropertiestext = dishpropertiestext & "<div id=""tip-" & objRds_properties("id") & """ class=""tip-content hidden""> "
dishpropertiestext=  dishpropertiestext &    htmltooltip
dishpropertiestext = dishpropertiestext & "</div>"
end if                                                  
'' end 
objRds_properties.MoveNext
wend 
objRds_properties.close()
set objRds_properties = nothing
End if

                                  
' code to check if toppings are applicable to this product
dishtoppingstext=""
'if (miallowtoppings & "" <> "0" and trim( miallowtoppings & "") <> "") or ( mipallowtoppings & "" <> "0" and trim( mipallowtoppings & "") <> "")  then
if ToppingGroupIDs & "" <> "" or MToppingGroupIDs & "" <> ""  then
dim listtoppinggroupid : listtoppinggroupid = ""
if trim( miallowtoppings & "") <> "0" and trim( miallowtoppings & "") <> ""   then
listtoppinggroupid = miallowtoppings
end if

if trim( mipallowtoppings & "") <> "0" and  trim( mipallowtoppings & "") <> ""  then
if listtoppinggroupid = "" then
listtoppinggroupid =mipallowtoppings
else
listtoppinggroupid =listtoppinggroupid &  "," & mipallowtoppings
end if
end if
if ToppingGroupIDs & "" <> "" then
listtoppinggroupid =  ToppingGroupIDs
else
listtoppinggroupid = MToppingGroupIDs
end if    

Set objRds_toppings_Group = Server.CreateObject("ADODB.Recordset")  
 SQL = "select ID,i_displaysort,isnull(limittopping,0) as limittopping "
if cookieposition = 1 then
    SQL = SQL & ",toppingsgroup "
else
    SQL = SQL & ",toppingsgroup"& cookieposition &" AS toppingsgroup "
end if
SQL = SQL & " from Menutoppingsgroups where IdBusinessDetail = " &   vRestaurantId & " and ID in (" &listtoppinggroupid& ")  order by i_displaysort,id "                                                     
objRds_toppings_Group.Open SQL, objCon
while not objRds_toppings_Group.EOF 
Set objRds_toppings = Server.CreateObject("ADODB.Recordset")           
 if cookieposition = 1 then        
    SQL = "SELECT id,topping,toppingprice,i_displaysort,s_ContainAllergen,s_MayContainAllergen,s_SuitableFor FROM MenuToppings with(nolock)    where  IdBusinessDetail=" & vRestaurantId                                               
else
    SQL = "SELECT id,isnull(topping"& cookieposition &",'') as topping,toppingprice,i_displaysort,s_ContainAllergen,s_MayContainAllergen,s_SuitableFor FROM MenuToppings with(nolock)    where  IdBusinessDetail=" & vRestaurantId                                               
end if
SQL =SQL & " and toppinggroupid=" & objRds_toppings_Group("ID")    & " order by i_displaysort,id "                                            
objRds_toppings.Open SQL, objCon
if dishtoppingstext & "" <> "" then
dishtoppingstext = dishtoppingstext & "<br/>"
end if
'dishtoppingstext =  "<div class=""dishproperties__title"">" & objRds_toppings_Group("toppingsgroup") & " </div> "
dishtoppingstext =dishtoppingstext &  "<div class=""dishproperties__title"">" & objRds_toppings_Group("toppingsgroup") & " </div> "
dim s_ContainAllergen_t,s_MayContainAllergen_t,s_SuitableFor_t 
While NOT objRds_toppings.Eof 
s_ContainAllergen_t = Replace(objRds_toppings("s_ContainAllergen")& ""," ","") 
s_MayContainAllergen_t  =Replace( objRds_toppings("s_MayContainAllergen")&""," ","")
s_SuitableFor_t  = Replace( objRds_toppings("s_SuitableFor")&""," ","")
dim shtmlicons : shtmlicons = ""

if s_ContainAllergen_t & "" <> "" then 
''FindAllergen
dim   arr_s_ContainAllergen_t : arr_s_ContainAllergen_t = split(s_ContainAllergen_t,",")
index_m = 0
for index_m = 0 to ubound(arr_s_ContainAllergen_t)
s_contain = FindAllergen(arr_s_ContainAllergen_t(index_m) )
if s_contain & "" <> "" then
shtmlicons = shtmlicons &  "<img width=""17"" data-toggle=""tooltip""  height=""17"" src=""" & SITE_URL & "Images/allergen/png/" & replace(split(s_contain,"|")(2),"amber","red") & """    title=""" & FilterData(arrData,"scontains") & " " & split(s_contain,"|")(1)&"""  alt="""& FilterData(arrData,"scontains") & " " & split(s_contain,"|")(1)&""" />"
end if
next
end if

if s_MayContainAllergen_t & "" <> "" then 
''FindAllergen
dim   arr_s_MayContainAllergen_t : arr_s_MayContainAllergen_t = split(s_MayContainAllergen_t,",")
index_m = 0
for index_m = 0 to ubound(arr_s_MayContainAllergen_t)
s_contain = FindAllergen(arr_s_MayContainAllergen_t(index_m) )
if s_contain & "" <> "" then
shtmlicons = shtmlicons &  "<img width=""17"" height=""17"" data-toggle=""tooltip""  src=""" & SITE_URL & "Images/allergen/png/" & split(s_contain,"|")(2) & """    title=""" & FilterData(arrData,"may_contain") & " " &split(s_contain,"|")(1)&"""  alt=""" & FilterData(arrData,"may_contain") & " " & split(s_contain,"|")(1)&""" />"
end if
next
end if

if s_SuitableFor_t & "" <> "" then 
''FindAllergen
dim   arr_s_SuitableFor_t : arr_s_SuitableFor_t = split(s_SuitableFor_t,",")
index_m = 0
for index_m = 0 to ubound(arr_s_SuitableFor_t)
s_contain = FindAllergen(arr_s_SuitableFor_t(index_m) )
if s_contain & "" <> "" then
shtmlicons = shtmlicons &  "<img width=""17"" height=""17"" data-toggle=""tooltip""  src=""" & SITE_URL & "Images/allergen/png/" & split(s_contain,"|")(2) & """    title="""&split(s_contain,"|")(1)&"""  alt="""&split(s_contain,"|")(1)&""" />"
end if
next
end if

dishtoppingstext = dishtoppingstext &  "<span s_ContainAllergen_t="""  & s_ContainAllergen_t & "|" & s_MayContainAllergen_t & """  s_MayContainAllergen_t="""  & s_MayContainAllergen_t & """  s_SuitableFor_t="""  & s_SuitableFor_t & """  class=""topping d-flex""> " 
dishtoppingstext = dishtoppingstext &  " <span class=""mr-5""> " 
dishtoppingstext = dishtoppingstext &  "    <input type=""checkbox"" toppinggroup=""topping_" & objRds_toppings_Group("ID") & vMenuItemId & "-" & PropertyId  &""" name=""" & objRds_toppings("topping") & """ value=""" & objRds_toppings("id") & """ data-group=""toppings" & vMenuItemId & "-" & PropertyId & """> " & objRds_toppings("topping") & shtmlicons 
dishtoppingstext = dishtoppingstext &  " </span> " 
dishtoppingstext = dishtoppingstext &  " <span  class=""ml-auto""> " 
dishtoppingstext = dishtoppingstext &   formatcurentcyC( FormatNumber(objRds_toppings("toppingprice"),2)  )
dishtoppingstext = dishtoppingstext &  " </span> " 
dishtoppingstext = dishtoppingstext &  "</span>"
objRds_toppings.MoveNext
wend 
objRds_toppings.close()
set objRds_toppings = nothing
if cint( objRds_toppings_Group("limittopping")) > 0 then
dishtoppingstext  = dishtoppingstext & "<script>checkboxlimit('topping_" & objRds_toppings_Group("ID") & vMenuItemId & "-" & PropertyId  & "'," &  objRds_toppings_Group("limittopping")  &  ");</script>"
end if
objRds_toppings_Group.movenext()
wend
objRds_toppings_Group.close()
set objRds_toppings_Group = nothing
end if
noprice=0
If Not IsNull(MenuPrice) and donotshowprice="n" Then %>
<div class="product-line__price"><b><%=formatcurentcyC( FormatNumber(MenuPrice, 2)) %></b></div> 
                                
<%  noprice=1
End If %>							
<%if pricefrom & "" <> "0" then%>                               
<div class="product-line__price"><b><%=FilterData(arrData,"sfrom") %>&nbsp;<%=formatcurentcyC( FormatNumber(pricefrom, 2)) %></b></div>    
<%noprice=1
end if%>
<div class="product-line__action-btn">	
<div align="right">
<% if dishpropertiestext & "" <> ""  or dishtoppingstext & "" <> "" then  %>
<button class="btn btn-success"
data-toggle="modal" data-target="#dishproperties<%=vMenuItemId %>-<%=objRds_MenuItem("PropertyId") %>"
onclick="Showdishproperties('dishproperties<%=vMenuItemId %>-<%=objRds_MenuItem("PropertyId") %>');">
<span style="top:2px;" class="glyphicon glyphicon-plus-sign"></span>
</button>
<% else %>
<button class="btn btn-success btnadd" onclick="Add(<%=vMenuItemId %>,<%=PropertyId %>,this);">
<span class="glyphicon glyphicon-plus"></span>
<span class="fa fa-refresh fa-spin" aria-hidden="true" style=" width: 1em;display:none;"></span>
</button>    
<% end if %>
</div>					
</div>
</div>
</div>
</div>                                    
<%if dishpropertiestext<>"" or dishtoppingstext<>"" then%>
<div class="modal fade optionProductModal" id="dishproperties<%=vMenuItemId %>-<%=objRds_MenuItem("PropertyId") %>">
<div class="modal-dialog modal-md">
    <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            ×</button>
        <div class="modal-body">
            <div class="row dishproperties__inner">
                <%if dishtoppingstext<>"" then%>
                <div class="dishproperties__heading">    
                <b>FilterData(arrData,"toppings")</b>
                </div>
                <%=dishtoppingstext%>
                <%end if%>
                <%if dishpropertiestext<>"" then%>
                <div class="dishproperties__heading" name="dishproperties__heading">
                <b>Dish Options</b>
                </div>
                <%=dishpropertiestext%>
                <script type="text/javascript">
                $(function(){
                $('.tip').each(function () {
                $(this).tooltip(
                {
                html: true,
                title: "<div class='tooltip-custom'>" + $('#' + $(this).data('tip')).html().trim() + "</div>",
                container: 'body'
                });
                });                                                    
                });
                </script>
                <%end if%>
                
                </div>
        </div>
        <div class="modal-footer">
            <div class="dishproperties__btn is-vertical-center">
                <button class="btn btn-success btnadd" onclick="Add(<%=vMenuItemId %>,<%=PropertyId%>,this);">
                <span class="glyphicon glyphicon-plus"></span>
                <span class="fa fa-refresh fa-spin" aria-hidden="true" style=" width: 1em;display:none;"></span>
                </button>
                </div>
        </div>
    </div>
</div>
</div>					
<%end if%>
<%
objRds_MenuItem.MoveNext 
wend
%>
</div>
<%
'end 
objRdsMainCategory.MoveNext
wend
objRds_MenuItem.Close()
set objRds_MenuItem = nothing
objRdsMainCategory.close()
set objRdsMainCategory = nothing
%> 