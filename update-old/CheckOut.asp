
<%
 
     if Session("ResID") & "" <> "" then            
        session("restaurantid") = Session("ResID")
        Session("ResID") = ""
    else
         if request.querystring("id_r") & "" <> "" then
            session("restaurantid") = request.querystring("id_r")
        end if
    end if

    if Request.QueryString("lg") & "" <> "" then
        Response.Cookies("language") = Request.QueryString("lg")
	    Response.Cookies("language").Expires = Date() + 1 
    end if
   
if session("restaurantid")="" then
     if request.querystring("id_r") & "" <> "" then
        response.redirect(SITE_URL & "menu.asp?id_r=" & request.querystring("id_r") & "&timeout=yes")
      else 
        response.redirect(SITE_URL & "error.asp")
    end if
end if
   function FirstCapitalize(byval str)
    Dim result : result = str
        if trim(str & "") <> "" then
            result = ucase(left(str,1)) & Mid(str,2,len(str)-1)
        end if
    FirstCapitalize  = result
   end function
    %>

<!-- #include file="Config.asp" -->
<!-- #include file="timezone.asp" -->
<!-- #include file="restaurantsettings.asp" -->


<!DOCTYPE html>
<html dir="rtl" lang="ar">
<head>
  <meta charset="utf-8">
  <title>Checkout</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

	<!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
	<!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
	<!--script src="Scripts/less-1.3.3.min.js"></script-->
	<!--append ‘#!watch’ to the browser URL, then refresh the page. -->
	
	<!-- <link href="<%=SITE_URL %>css/bootstrap.min.css" rel="stylesheet"> -->
	<link href="<%=SITE_URL %>css/bootstrap-rtl.css" rel="stylesheet">
	<!-- <link href="<%=SITE_URL %>css/style.css?v=1" rel="stylesheet">     -->
	<link href="<%=SITE_URL %>css/style-rtl.css?v=1" rel="stylesheet">    
	<link href="<%=SITE_URL %>css/datepicker.css" rel="stylesheet">
    <!--<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">-->
    <link href=" //stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@400;600;700&display=swap" rel="stylesheet">
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="Scripts/html5shiv.js"></script>
  <![endif]-->

  <!-- Fav and touch icons -->
 <% If FAVICONURL & "" <> "" Then %> <link rel='shortcut icon' href='<%=FAVICONURL %>' type='image/x-icon'/ > <% end If %>
  
	<script type="text/javascript" src="<%=SITE_URL %>Scripts/jquery.min.js"></script>
	<script type="text/javascript" src="<%=SITE_URL %>Scripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="<%=SITE_URL %>Scripts/scripts.js"></script>
	<script  defer type="text/javascript" src="<%=SITE_URL %>Scripts/jquery.lazy.min.js"></script>
    <script src="<%=SITE_URL %>Scripts/jquery.validate.min.js" type="text/javascript"></script>
    <script defer type="text/javascript" src="<%=SITE_URL %>scripts/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>	
    <script src="<%=SITE_URL %>Scripts/bootstrap-datepicker.js" type="text/javascript"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<%= GMAP_API_KEY %>&sensor=false"></script>
 <style type="text/css">
        small.error 
        {
            display: inline;    
            color: #B94A48; 
        }
		#wholepage {
padding-top:0px !important;
}
        .hightlight1 {
            border-color: rgb(102, 175, 233);
	        outline: 0px none;
	        box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.075) inset, 0px 0px 8px rgba(102, 175, 233, 0.6);
        }
        /*.divider-or {margin-top:0px!important;}*/
        .tooltip .tooltip-inner{
           width: auto;
          max-width: 100%;
          padding-top:5px;
          padding-bottom: 5px;
          text-align: left;
          font-size: 12px;
        }
        .product-line__content-right, .product-line__content-left {
            padding: 2px 0;
        }
        .d-flex-center{
          display: flex;
          align-items: center;
        }
        .d-flex-end{
          justify-content: flex-end;
        }
        .product-lineprice.product-lineprice{
        margin-right: 10px;
        }

        
.photo img {
        width: 30px  !important;
    }

    .photo .overlay{
        width: 100% !important;
        max-width: 100% !important;
    }
@media (min-width: 768px){

    .photo img {
        width: 90px !important;
    }

}
    </style>

<% 

    function ShippingFee(byval DistanceMile,byval UserDistance, byval DeliveryFeev, byval freeDistnace,byval DeliveryCostUpTo, byval DeliveryUptoMile)
   
            Dim Ratio,numbermod  
            Dim Result : Result = 0
          '  Response.Write("DeliveryFeev " & DeliveryFeev & " DistanceMile " & DistanceMile & " UserDistance " & UserDistance & " freeDistnace " & freeDistnace & "<br/>")
            if UserDistance & "" = "" then
                    UserDistance = 0
            end if
             ' Response.Write("UserDistance " & UserDistance & " freeDistnace " & freeDistnace & "<br/>")
            if cdbl(UserDistance) <= cdbl(freeDistnace) and cdbl(freeDistnace) > 0 then
                Result = 0
            elseif ( DistanceMile & "" <> "" and DistanceMile & "" <> "0")  or (cdbl(DeliveryCostUpTo) > 0 and   cdbl(DeliveryUptoMile) > 0)  then
               
                'UserDistance =cdbl( UserDistance) - cdbl(freeDistnace)
                UserDistance =cdbl( UserDistance)
                dim DeliveryExtraCost : DeliveryExtraCost = 0
                if  cdbl( DeliveryCostUpTo) > 0 and  cdbl( DeliveryUptoMile) > 0 then
                    if cdbl( UserDistance) > cdbl(DeliveryUptoMile) then
                        UserDistance =cdbl( UserDistance) - cdbl(DeliveryUptoMile)
                        DeliveryExtraCost = cdbl(DeliveryCostUpTo)
                    else
                        UserDistance = 0
                        DeliveryExtraCost = cdbl(DeliveryCostUpTo)
                    end if

                end if  
        
                DistanceMile = cdbl(DistanceMile)
                if (UserDistance * 100) mod (DistanceMile * 100) > 0  then
                    Ratio = 1 + ( UserDistance * 100 - ((UserDistance * 100) mod (DistanceMile * 100))) / (DistanceMile * 100 )
                else
                    Ratio = ( UserDistance * 100 ) / (DistanceMile * 100)
                end if
                      
                  Result = Ratio * DeliveryFeev + DeliveryExtraCost
  
                       ' Response.End
            else
                   Result = distancefee
            end if
            'Response.Write("Result " & Result & "<br/>")
            ShippingFee = Result

    end function 

    Set objCon = Server.CreateObject("ADODB.Connection")
    Set objRds = Server.CreateObject("ADODB.Recordset") 

    Dim vRestaurantId
    dim vOrderId
    dim vOrderShipTotal
    dim vOrderSubTotal
    dim vOrderTotal
    Dim sPostalCode
    Dim sDeliveryDistance
    Dim sDeliveryFreeDistance
    Dim vaveragecol

    vRestaurantId = session("restaurantid") 
      '' Get Url Menu, checkout , thanks
    dim MenuURL,CheckoutURL,ThankURL
     objCon.Open sConnString
    MenuURL =  SITE_URL & "menu.asp?id_r=" & vRestaurantId
    if vRestaurantId & "" <> "" then
           dim rs_url :  set rs_url = Server.CreateObject("ADODB.Recordset")
               rs_url.open "SELECT FromLink FROM URL_REWRITE a   inner join BusinessDetails b   on (a.RestaurantID=b.ID )  where RestaurantID=" & vRestaurantId & " and EnableUrlRewrite = 'Yes' and status = 'ACIVE' " ,objCon
            while not rs_url.eof 
               
               if instr(lcase(rs_url("FromLink")),"/menu") > 0 then
                     MenuURL = rs_url("FromLink")
               elseif  instr(lcase(rs_url("FromLink")),"/checkout") > 0 then
                     CheckoutURL = rs_url("FromLink")
               elseif instr(lcase(rs_url("FromLink")),"/thanks") > 0 then
                     ThankURL = rs_url("FromLink")
               end if 
               rs_url.movenext()
           wend
            rs_url.close()
        set rs_url =  nothing
        if instr( lcase(SITE_URL) ,"https://") then
            MenuURL  = replace(MenuURL,"http://","https://")    
            CheckoutURL  = replace(CheckoutURL,"http://","https://")    
            ThankURL  = replace(ThankURL,"http://","https://")    
         end if  
    end if
    '' end 

    Dim SQLDetails
    SQLDetails = "select bd.Id,bd.EnableDeliveryTimeSlot,bd.EnableCollectionTimeSlot,bd.enablecaching,bd.Close_StartDate,bd.Close_EndDate,bd.s_BannerURL, "
    SQLDetails = SQLDetails & "bd.s_IconApple,bd.s_UrlApple,bd.s_IconGoogle,bd.s_UrlGoogle,bd.EnableAllergen,bd.EnableSuitableFor,bd.enablereorder,bd.EnableBooking,bd.PostalCode, "
    SQLDetails = SQLDetails & "bd.orderonlywhenopen,bd.disablelaterdelivery,bd.individualpostcodeschecking,bd.googleecommercetrackingcode, "
    if cookieposition = 1 then
        SQLDetails = SQLDetails & "bd.menupagetext,bd.Name,bd.FoodType,bd.Address,term_of_trade, "
    Else
        SQLDetails = SQLDetails & "bd.menupagetext"& cookieposition &" AS menupagetext,bd.Name"& cookieposition &" AS [Name],bd.Address"& cookieposition &" AS [Address],bd.FoodType"& cookieposition &" AS FoodType,term_of_trade"& cookieposition & " as term_of_trade, "
    end if
    if cookieposition = 1 then
        SQLDetails = SQLDetails & "bd.announcement,bd.inmenuannouncement,bd.announcement_Filter, "
    Else
        SQLDetails = SQLDetails & "bd.announcement"& cookieposition &" AS announcement,bd.inmenuannouncement"& cookieposition &" AS inmenuannouncement,bd.announcement_Filter"& cookieposition &" AS announcement_Filter, "
    end if
    SQLDetails = SQLDetails & "bd.DeliveryMile,bd.DeliveryUptoMile,bd.DeliveryCostUpTo,bd.AverageDeliveryTime,bd.AverageCollectionTime, "
    SQLDetails = SQLDetails & "bd.s_DeliveryZonesPath,bd.individualpostcodes,bd.DeliveryMaxDistance,bd.DeliveryFreeDistance,bd.DeliveryMinAmount,bd.DeliveryMinAmount,bd.DeliveryFee, "
    SQLDetails = SQLDetails & "bd.DeliveryChargeOverrideByOrderValue,bd.ImgUrl,bd.Telephone, bd.Email "
    SQLDetails = SQLDetails & " FROM BusinessDetails bd with(nolock)  WHERE Id = " & vRestaurantId
   
    objRds.Open SQLDetails, objCon
    sPostalCode = objRds("PostalCode")
    sDeliveryMaxDistance = Cdbl(objRds("DeliveryMaxDistance"))
    sDeliveryFreeDistance= Cdbl(objRds("DeliveryFreeDistance"))
    vaveragecol = objRds("AverageCollectionTime")
    sDeliveryChargeOverrideByOrderValue = 1000000000
    individualpostcodeschecking=objRds("individualpostcodeschecking")
    dim googleecommercetrackingcode
    googleecommercetrackingcode = objRds("googleecommercetrackingcode")
    term_of_trade = objRds("term_of_trade")

    if Not isnull(objRds("DeliveryChargeOverrideByOrderValue")) Then
	    sDeliveryChargeOverrideByOrderValue= Cdbl(objRds("DeliveryChargeOverrideByOrderValue"))
    End If

    Dim DeliveryCostUpTo : DeliveryCostUpTo = objRds("DeliveryCostUpTo") & ""
    if DeliveryCostUpTo = "" then
        DeliveryCostUpTo = 0
    end if
   
    Dim DeliveryUptoMile : DeliveryUptoMile = objRds("DeliveryUptoMile") & ""
     if DeliveryUptoMile = "" then
        DeliveryUptoMile = 0
    end if
    Dim DeliveryFee : DeliveryFee = objRds("DeliveryFee") & ""
    if DeliveryFee <> "" then
       DeliveryFee  =cdbl(DeliveryFee)
    else
        DeliveryFee = 0 
    end if
    Dim DistanceMile : DistanceMile = objRds("DeliveryMile") & ""

    if DistanceMile <> "" then
       DistanceMile  =cdbl(DistanceMile)
    else
        DistanceMile = 0 
    end if

    vOrderShipTotal = ShippingFee( DistanceMile, Request.Form("deliveryDistance"),DeliveryFee,sDeliveryFreeDistance,DeliveryCostUpTo,DeliveryUptoMile)
    
     
     
     Dim FirstNameCustomer,LastNameCustomer,EmailCustomer,PostaLCodeCustomer,AddressCustomer,Telno
   
    if Request.Cookies("FormAddress") & ""  <> "" and Request.Cookies("FormPostCode") & "" <> ""  then
        FirstNameCustomer = replace(Request.Cookies("FormFirstName") & "","[space]"," ")
        LastNameCustomer = replace(Request.Cookies("FormLastName") & "","[space]"," ")
        EmailCustomer = replace(Request.Cookies("FormEmail") & "","[space]"," ")
       ' Response.Write("EmailCustomer " & EmailCustomer & " <br/> ")
        Telno = replace( Request.Cookies("FormPhoneNumber") & "","[space]"," ")
        AddressCustomer  = replace( Request.Cookies("FormAddress") & "","[space]"," ")
       ' Response.Write(AddressCustomer)
        PostaLCodeCustomer = replace(Request.Cookies("FormPostCode") & "","[space]"," ")
       ' Response.Write("AddressCustomer " & AddressCustomer & " <br/>")
        if AddressCustomer& ""  <> "" then
            dim arrAddressCustomer : arrAddressCustomer =  split(AddressCustomer,",")
            dim lindex : lindex = 0 
             for  lindex = 0 to ubound(arrAddressCustomer)
                if arrAddressCustomer(lindex) & "" <> "" then
                    if lindex = ubound(arrAddressCustomer) then                  
                        Address2Customer = arrAddressCustomer(lindex)
                    else
                        Address1Customer = Address1Customer & arrAddressCustomer(lindex) & ","
                    end if
                end if
                
             next
      
             if Address1Customer & "" <> "" then
                Address1Customer  = left(Address1Customer,len(Address1Customer)-1)
             end if

          
             if Address1Customer <> "" then
                lindex = 0
                dim arrAddress1Customer : arrAddress1Customer = split(Address1Customer," ")
                Address1Customer= ""
                for  lindex = 0 to ubound(arrAddress1Customer)
                    if HouseNumberCustomer  ="" then
                        HouseNumberCustomer = arrAddress1Customer(lindex)
                    else
                        Address1Customer = Address1Customer & arrAddress1Customer(lindex) & " "
                    end if     
                next
              end if
            
        end if
     
    else
        FirstNameCustomer = Request.Cookies("firstname") & "" 
        LastNameCustomer = Request.Cookies("LastName") & ""
        EmailCustomer = Request.Cookies("Email") & ""
        PostaLCodeCustomer =  Request.Cookies("PostCode" & vRestaurantId) & ""
       ' AddressCustomer = RSCustomerInfo("PostalCode") & "" 
        Telno =  Request.Cookies("Phone") & ""
        HouseNumberCustomer = Request.Cookies("HouseNumber" & vRestaurantId) & "" 
        Address1Customer = Request.Cookies("Address" & vRestaurantId) & "" 
        Address2Customer = Request.Cookies("Address2"&vRestaurantId) & "" 
    end if
%>
<body onunload="">

<!-- Safari iOS reload page, without loading from cache -->
<iframe style="height:0px;width:0px;visibility:hidden" src="about:blank">
    this frame prevents back forward cache
</iframe>


<script>
$(window).bind("pageshow", function(event) {
    if (event.originalEvent.persisted) {
        window.location.reload() 
    }
});
</script>


<input type="hidden" id="refreshed" value="no">
<script type="text/javascript">
onload=function(){
var e=document.getElementById("refreshed");
if(e.value=="no")e.value="yes";
else{e.value="no";location.reload();}
}
</script>
<!-- Safari iOS reload page, without loading from cache -->
<div class="container" id="wholepage" style="padding-bottom:100px;">
	<div class="row clearfix headerbox" id="header">
        <div class="col-md-12 col-xs-12" style="padding-bottom:10px;" id="topmenumobile">
            <div class="media">
                 <a href="#" class="pull-left"><img src="<%= objRds("ImgUrl") %>" width=70 class="media-object" alt="<%= objRds("Name") %>"></a>
                <div class="media-body">
                    <h4 class="media-heading">                
                <div style="float:left;">                
                <div class="hidden-xs u-display-block">
          <% if URL_Facebook & "" <> "" or _ 
              URL_Twitter & "" <> "" or _  
              URL_Google & "" <> "" or _  
              URL_Linkin & "" <> "" or _ 
              URL_YouTube & "" <> "" or _ 
              URL_Intagram & "" <> "" or _ 
              URL_Tripadvisor & "" <> "" then  %>
        <div class="social-header dis-hide">
            <% if URL_Facebook & "" <> "" then  %>
            <a href="<%=URL_Facebook %>" title="facebook" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
            <% end if %>
            <% if URL_Twitter & "" <> "" then  %>
            <a href="<%=URL_Twitter %>" title="twitter"  target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
            <% end if %>
            <% if URL_Google & "" <> "" then  %>
            <a href="<%=URL_Google %>" title="google plus"  target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
            <% end if  %>
            <% if URL_Linkin & "" <> "" then  %>
            <a href="<%=URL_Linkin %>" title="linkedin"  target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
            <% end if %>
             
            <% if URL_Intagram & "" <> "" then  %>
            <a href="<%=URL_Intagram %>" title="instagram"  target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
            <% end if %>
              <% if URL_YouTube & "" <> "" then  %>
            <a href="<%=URL_YouTube %>" title="youtube"  target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a>
            <% end if %>
             
              <% if URL_Tripadvisor & "" <> "" then  %>
            <a href="<%=URL_Tripadvisor %>" title="tripadvisor"  target="_blank"><i class="fa fa-tripadvisor" aria-hidden="true"></i></a>
            <% end if %>
        </div>

        <% end if %>
                <i class="fa fa-phone"></i> <%= objRds("Telephone") %> 
<span class="glyphicon glyphicon glyphicon-envelope"></span>  <%= objRds("Email") %></div>

<div class="visible-xs icon-thumb">

    <script>
       var isconfirm = false;
       
        function confimBookTable()
        {
            if(isconfirm==false)
               isconfirm = confirm("<%=FilterData(arrData,"note") & " " %> <%=FilterData(arrData,"you_can_add_food_to_your_table_booking") %>");
            var htmlItem = "";
            if(isconfirm==true)
                {
                    
                      //  listitemincart

                    htmlItem="";
                    $("#divShoppingCartSroll table tr").each(function(){
                            htmlItem += "<tr>";
                                    htmlItem += "<td name=\"itemName\">" + $(this).find("[name=itemName]").html()  + "</td>";
                                    htmlItem += "<td name=\"itemPrice\" style=\"vertical-align:top\">" + $(this).find("[name=itemPrice]").html() + "</td>";
                            htmlItem += "</tr>";
                    });
                    if(htmlItem!="")
                        {
                            var wrapItem ="";
                            wrapItem="<div class=\"panel panel-primary\">";
                            wrapItem+="    <div class=\"panel-heading\">";
                            wrapItem+="        <h3 class=\"panel-title\"><span class=\"glyphicon glyphicon glyphicon-shopping-cart\"></span><%=FilterData(arrData,"your_order")%></h3>";
                            wrapItem+="    </div> ";
                            wrapItem+="    <div class=\"panel-body\" style=\"padding:15px 8px 15px 8px;\">";
                            wrapItem+="        <div> ";
                            wrapItem+="            <div class=\"shoppingCartScroll\"> ";
                            wrapItem+="                <table style=\"width: 100%\"> ";
                            wrapItem+="                    <tbody> ";
                            wrapItem+=htmlItem;

                            wrapItem+="                     </tbody> ";
                            wrapItem+="                 </table> ";
                            wrapItem+="             </div> ";
             
                            wrapItem+="         </div> ";
                            wrapItem+="     </div> ";
                            wrapItem+=" </div>";
                            htmlItem = wrapItem;
                        }
                      
                        //htmlItem = "<div class=\"w3-padding w3-white notranslate\"><table class=\"table table-bordered\"><tbody>" + htmlItem + "</tbody></table></div>";
                }
            if(htmlItem=="")
        
                htmlItem = "<div class='row'><div class='col-md-1 col-xs-1'><i class='fa'>&#xf022;</i></div> <div class='col-md-11 col-xs-11'><label style='color:darkolivegreen ;'><%=FilterData(arrData,"you_can_add_food_to_your_table_booking")%></label></div> </div>";
            $("#listitemincart").html(htmlItem);
            return true;
        }
        
    </script>
<a href="https://www.google.co.uk/maps?q=<%= objRds("Address") %>" target="_blank"><span class="glyphicon glyphicon-map-marker"></span></a>
<a href="tel:<%= objRds("Telephone") %>"><span class="glyphicon glyphicon-earphone"></span></a>
<a href="mailto:<%= objRds("Email") %>"><span class="glyphicon glyphicon-envelope"></span></a>
<a href="#" title="gift" class="social-icon-visible"><span><i class="fa fa-gift" aria-hidden="true"></i></span></a>
   <% if URL_Facebook & "" <> "" or _ 
              URL_Twitter & "" <> "" or _  
              URL_Google & "" <> "" or _  
              URL_Linkin & "" <> "" or _ 
              URL_YouTube & "" <> "" or _ 
              URL_Intagram & "" <> "" or _ 
              URL_Tripadvisor & "" <> "" then  %>

<div class="social-thumb dis-hide">
  <span class="social-text">|</span>
    <% if URL_Facebook & "" <> "" then %>
  <a href="<%=URL_Facebook %>" title="facebook" class="social-icon"  target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
    <% end if %>
        <% if URL_Twitter & "" <> "" then %>
  <a href="<%=URL_Twitter %>" title="twitter" class="social-icon"  target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
    <% end if %>
         <% if URL_Google & "" <> "" then %>
  <a href="<%=URL_Google %>" title="google plus" class="social-icon"  target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
    <% end if %>
       <% if URL_Linkin & "" <> "" then %>
  <a href="<%=URL_Linkin %>" title="linkedin" class="social-icon"  target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
    <% end if %>
    <% if URL_Intagram & "" <> "" then %>
  <a href="<%=URL_Intagram %>" title="instagram" class="social-icon"   target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
    <% end if %>
     <% if URL_YouTube & "" <> "" then %>
  <a href="<%=URL_YouTube %>" title="youtube" class="social-icon"   target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a>
    <% end if %>
     
     <% if URL_Tripadvisor & "" <> "" then %>
  <a href="<%=URL_Tripadvisor %>" title="tripadvisor" class="social-icon"   target="_blank"><i class="fa fa-tripadvisor" aria-hidden="true"></i></a>
    <% end if %>
</div>
<% end if %>
</div>



</div>  
                         <%= objRds("Name") %>
                         

                    </h4><div class="hidden-xs">
                            <b><%= objRds("Address") %> </b><br>
                        </div>


<%= objRds("FoodType") %>
                    
            </div>
            </div>
        </div>
         

    </div> <!-- end header -->




       

        
    <%            
        objRds.Close      
        set  objRds =  nothing 
        'objCon.Close

        if Session.SessionID & "" = "" then
             objCon.close()
            set objCon = nothing
             response.redirect(SITE_URL & "error.asp")
        end if
        'objCon.Open sConnString
        set objRds = Server.CreateObject("ADODB.Recordset")
        objRds.Open "select o.* from [Orders] o  " & _
            " Where o.IdBusinessDetail = " & vRestaurantId & _
            " And o.SessionId = '" & Session.SessionID & "'", objCon, 1, 3 
            
			discountcodeused=""
        vouchercode = ""
    if objRds.EOF then
            objRds.close()
        set objRds =  nothing
            objCon.close()
        set objCon = nothing
        response.redirect(SITE_URL &  "error.asp")
    end if
        dim VoucherDiscontType : VoucherDiscontType =""
	if objRds("vouchercodediscount") <> 0 or objRds("vouchercode") & "" <> ""  then
	    discountcodeused= "-" & objRds("vouchercodediscount") & "%"
        vouchercode = objRds("vouchercode")
        VoucherDiscontType = objRds("DiscountType")
	end if
			
        vOrderId = objRds("Id")
        vOrderSubTotal = cdbl(objRds("SubTotal"))
		
		 ' Response.Write("Mod " & (100 * 2.3 mod 100 * 0.5 ) & "<br/>")
          'Response.Write("UserDistance " & Request.Form("deliveryDistance") & " sDeliveryFreeDistance " & sDeliveryFreeDistance & " Delivery Fee " & DeliveryFee & " distance mile " & DistanceMile & "<br/>")
     
        If Request.Form("deliveryType") <> "d" Then
            vOrderShipTotal = 0
        elseIf Request.Form("deliveryDistance") <> "" and cdbl(sDeliveryFreeDistance) > 0 Then
        'elseIf Request.Form("deliveryDistance") <> ""  Then
            dim UserDistance : UserDistance = cdbl(Request.Form("deliveryDistance"))
            vOrderShipTotal =  ShippingFee( DistanceMile, UserDistance,DeliveryFee,sDeliveryFreeDistance,DeliveryCostUpTo,DeliveryUptoMile)
           ' Response.Write("vOrderShipTotal " & vOrderShipTotal & " sDeliveryFreeDistance " & sDeliveryFreeDistance & "<br/>")
            If UserDistance <= sDeliveryFreeDistance Then vOrderShipTotal = 0                              
        end if
   
		if cdbl( vOrderSubTotal) > cdbl( sDeliveryChargeOverrideByOrderValue) then
            
			vOrderShipTotal = 0
		end if
		'Response.Write("vOrderShipTotal " &vOrderShipTotal & " sDeliveryChargeOverrideByOrderValue " & sDeliveryChargeOverrideByOrderValue & "<br/>")
			
		Dim OrderDate, deliverytime, orderTotalAmount, serviceChargeAmount, extra_chargesAmount
            extra_chargesAmount = extra_charge
      
        Dim Duration : Duration = Request.Cookies("Duration") & ""
        if Duration & "" = "" then
            Duration = 0
        end if
        'Response.Write("extra_chargesAmount " & extra_chargesAmount  & "<br/><br/>")
        OrderDate =  DateAdd("h",houroffset,now)
        
        objRds("OrderDate") = DateAdd("h",houroffset,now)
        objRds("DeliveryType") = Request.Form("deliveryType")
        objRds("deliverydelay") = Request.Form("deliverydelay")
        objRds("collectiondelay") = Request.Form("collectiondelay")
        objRds("Duration") = Duration
        objRds("s_fingerprint") = Request.Form("s_fingerprint")
        
        if Request.Form("deliveryTime") & ""  <> "" and instr( trim(Request.Form("deliveryTime"))," ") > 0  then
		    coltimesplit=split(Request.Form("deliveryTime")," ")
		    coltime=coltimesplit(1)
		end if
        objRds("DeliveryTime") = JXIsoDate(Request.Form("deliveryTime")) + " " + coltime
        deliverytime = JXIsoDate(Request.Form("deliveryTime")) + " " + coltime
        if deliverytime & "" = "" then 
                 'objRds.close()
            set objRds =  nothing
                objCon.close()
            set objCon = nothing
            response.redirect(MenuURL)
        end if
        objRds("asaporder") = Request.Form("asaporder")
         objRds("PaymentSurcharge") = 0
        objRds("SubTotal") = vOrderSubTotal
        objRds("ShippingFee") = vOrderShipTotal
        objRds("OrderTotal") = vOrderSubTotal + vOrderShipTotal
        
        objRds("language") = cookielg
        objRds("positionlg") = cookieposition
   
        If ServiceChargePercentage & "" <> "" AND ServiceChargePercentage & "" <> "0" AND InRestaurantServiceChargeOnly = "0" Then
            objRds("ServiceCharge")  = (Cdbl(ServiceChargePercentage)*0.01*CDbl(vOrderSubTotal))
            'objRds("OrderTotal") = (Cdbl(ServiceChargePercentage)*0.01*CDbl(objRds("SubTotal"))) + CDbl(objRds("OrderTotal"))
        Else
            objRds("ServiceCharge") = 0
        End If
         if cdbl(extra_chargesAmount ) > 0 then
            objRds("extra_charges") = cdbl(extra_chargesAmount ) 
         else 
            objRds("extra_charges") = 0
         end if

        '' Calculate Tax 
             If Tax_Percent & "" <> "" AND Tax_Percent & "" <> "0" AND InRestaurantTaxChargeOnly = "0" Then
                objRds("Tax_Amount")  = (Cdbl(Tax_Percent)*0.01*CDbl(vOrderSubTotal + vOrderShipTotal))
                objRds("Tax_Rate")  =Tax_Percent
            Else
                objRds("Tax_Amount") = 0
                objRds("Tax_Rate")  =0
            End If
        '' End 
           '' Calculate Tip 
            Dim loyaltyamount : loyaltyamount = objRds("loyaltyamount")
            if loyaltyamount & "" = "" then
                loyaltyamount = 0 
            end if
            loyaltyamount = cdbl(loyaltyamount)
             Dim Tip_Rate : Tip_Rate = 0
                Tip_Rate = objRds("Tip_Rate") 
             If Tip_percent & "" <> "" AND Tip_percent & "" <> "0" AND InRestaurantTipChargeOnly = "0" Then
                 if objRds("Tip_Rate") & "" <> "custom" and objRds("Tip_Rate") & ""  <> "" then
                        Tip_percent = objRds("Tip_Rate")
                 end if
                if objRds("Tip_Rate") & "" <> "custom"   then
                     objRds("Tip_Amount")  = (Cdbl(Tip_Percent)*0.01*CDbl(vOrderSubTotal ))
                     objRds("Tip_Rate")  =Tip_percent
                    Tip_Rate = Tip_percent
                end if
            Else
                objRds("Tip_Amount") = 0
                'objRds("Tip_Rate")  =0
            End If
        dim TaxAmount,TipAmount
        '' End 
        TaxAmount = objRds("Tax_Amount")
        TipAmount = objRds("Tip_Amount")
        serviceChargeAmount =  objRds("ServiceCharge")
        extra_chargesAmount  = objRds("extra_charges")
        
        objRds("OrderTotal") = vOrderSubTotal + vOrderShipTotal + serviceChargeAmount + TaxAmount + TipAmount  + cdbl(extra_chargesAmount)
               orderTotalAmount = vOrderSubTotal + vOrderShipTotal + serviceChargeAmount + TaxAmount + TipAmount  + cdbl(extra_chargesAmount)
       Dim deliveryLat,deliveryLng
           deliveryLat =  Request.Form("deliveryLat") 
           deliveryLng = Request.Form("deliveryLng")
            if Request.Form("deliveryType") = "d" then
                    if deliveryLat & "" = "" and request.Form("hidLat") & "" <> ""   then
                                objRds("lng_report")  =  Request.Form("hidLng") 
                                objRds("lat_report")  =   Request.Form("hidLat") 
                     else
                                objRds("lng_report")  =  deliveryLng
                                objRds("lat_report")  =  deliveryLat 
                    end if
            end if
        objRds("DeliveryLat") = Request.Form("deliveryLat")
        objRds("DeliveryLng") = Request.Form("deliveryLng")

        objRds.Update 
    
            objRds.Close
        set objRds =  nothing
        'objCon.Close 
        'Response.Write("vOrderSubTotal " & vOrderSubTotal & " vOrderShipTotal " & vOrderShipTotal & " serviceChargeAmount " & serviceChargeAmount & " TaxAmount " & round(TaxAmount,2) & " TipAmount " & round(TipAmount,2) & "<br/>")
        vOrderTotal = vOrderSubTotal + vOrderShipTotal + round(serviceChargeAmount,2) + round(TaxAmount,2) + round(TipAmount,2)  + cdbl(extra_chargesAmount)
       
       
        Dim CollectedPoint : CollectedPoint = -1 
        dim earnpointthisorder : earnpointthisorder = 0
        if EmailCustomer & "" <> "" then
            Dim RS_Loyalty : set RS_Loyalty = Server.CreateObject("ADODB.Recordset")
            Dim SQL_Loyalty : SQL_Loyalty = " select isnull( sum(earn_point),0) as earn_point from Customer_loyalty  where email= '"  & EmailCustomer & "' and IdBusinessDetail= " & vRestaurantId
            RS_Loyalty.Open SQL_Loyalty,objCon,1,3
            if not  RS_Loyalty.EOF then
                CollectedPoint = cint(RS_Loyalty("earn_point")) 
              
            end if   
        end if
    %>

        
    <form id="frmMakeOrder" action="<%=SITE_URL %>MakeOrder.asp" method="post">
        <input type="hidden" name="Stripe_Token" id="Stripe_Token" value="" />
        <input type="hidden" name="order_id" value="<%= vOrderId%>"/>
        <input type="hidden" name="item_name" value="Order Nr. <%= vOrderId%>"/>
        <input type="hidden" name="amount" value="<%= FormatNumber(vOrderTotal, 2)%>"/>
        <input type="hidden" name="vOrderSubTotal" value="<%=vOrderSubTotal %>" />
         <input type="hidden" name="delivery_distance" value="<%= Request.Form("deliveryDistance")%>"/>
       <div class="row clearfix" >
			<div class="col-md-6  column" id="panel-left">
                <fieldset>
                <legend><%=FilterData(arrData,"personal_details") %></legend>
                <div class="control-group">
                    <label class="control-label" for="FirstName"><%=FilterData(arrData,"first_name") & " " %>*</label>
                    <div class="controls">
                        <input type="text" id="FirstName" name="FirstName" class="form-control" required placeholder="<%=FilterData(arrData,"your_first_name")%>" title="<%=FilterData(arrData,"this_field_is_required")%>" value="<%=FirstNameCustomer%>" />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="LastName"><%=FilterData(arrData,"last_name") & " " %> *</label>
                    <div class="controls">
                        <input type="text" id="LastName" name="LastName" class="form-control" required placeholder="<%=FilterData(arrData,"your_last_name")%>" title="<%=FilterData(arrData,"this_field_is_required")%>" value="<%=LastNameCustomer%>" />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="Email"><%=FilterData(arrData,"email_address") & " " %> *</label>
                    <div class="controls">
                        <input  id="Email" name="Email" class="form-control" required placeholder="<%=FilterData(arrData,"your_email_dddress")%>" title="<%=FilterData(arrData,"this_field_is_required")%>" value="<%=EmailCustomer%>"  type="email" />
                        <p id="hint"></p>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="Phone"><%=FilterData(arrData,"telephone") & " " %> *</label>
                    <div class="controls">
                      
                        <input id="Phone" name="Phone" class="form-control" required placeholder="<%=FilterData(arrData,"your_phone")%>" title="<%=FilterData(arrData,"this_field_is_required")%>" value="<%=Telno%>" type="number" pattern="\d+" />
                    </div>
                </div>

            </fieldset> 
                        <!--<script type="text/javascript" src="<%=SITE_URL %>Scripts/jquery-1.11.0.min.js"></script>-->
                        <script type="text/javascript" src="<%=SITE_URL %>Scripts/mailcheck.js"></script>
            			<script type="text/javascript">
            			    var email = jQuery('#Email');
            			    var hint = jQuery("#hint"); 
            			    email.on('blur',function() {
            			        hint.css('display', 'none').empty(); // hide hint initially
            			        jQuery(this).mailcheck({
            			            suggested: function(element, suggestion) {
            			                if(!hint.html()) {
            			                    // misspell - display hint element
                                            var suggestion = "<%=FilterData(arrData,"did_you_mean") & " " %>  <span class='suggestion'>" +
                                               "<span class='address'>" + suggestion.address + "</span>"
                                               + "@<a href='#' class='domain'>" + suggestion.domain +
                                               "</a></span>?";
 
            			                    hint.html(suggestion).fadeIn(150);
            			                } else {
            			                    // Subsequent errors
            			                    jQuery(".address").html(suggestion.address);
            			                    jQuery(".domain").html(suggestion.domain);
            			                }
            			            }
            			        });
            			    });
 
            			    hint.on('click', '.domain', function() {
            			        // Display with the suggestion and remove the hint
            			        email.val(jQuery(".suggestion").text());
            			        hint.fadeOut(200, function() {
            			            jQuery(this).empty();
            			        });
            			        return false;
            			    });
                        </script>
                <fieldset>
                
                    <% if COLLECTION_ENABLE_ADDRESS <> "Yes" or Request.Form("deliveryType") = "d"   then  %>
                    <legend><%=FilterData(arrData,"your_address") %></legend>
                    <%
                        Dim Add1, Add2, IsFromGoogle, FromGoogleHighlight, HouseNumber, PostCode
                        IsFromGoogle = true
                        FromGoogleHighlight = "" 'hightlight1
                        HouseNumber = ""
                   
                        IF HouseNumberCustomer & "" <> "" ANd PostaLCodeCustomer & "" <> "" AND (Request.Form("isChangeExistingAddress") & "" = "" or Request.Form("isChangeExistingAddress") & "" = "N")  Then
                            
                            PostCode = PostaLCodeCustomer 'Request.Cookies("PostCode")
                            HouseNumber =HouseNumberCustomer 'Request.Cookies("HouseNumber")
                            Add1 = Address1Customer 'Request.Cookies("Address")
                            Add2 = Address2Customer 'Request.Cookies("Address2")
                            
                        Else
                     '   Response.Write("ok2<br/>")
                         
                            If Request.Form("deliveryAddress") & "" <> "" AND InStr(Request.Form("deliveryAddress"),"[*]") > 1 Then
                                Dim tempArrAddress 
                                tempArrAddress = Split(Request.Form("deliveryAddress"),"[*]")
                        
                                If Ubound(tempArrAddress) = 2 Then
                                    HouseNumber = tempArrAddress(0)
                                    Add1 = tempArrAddress(1)
                                    Add2 = tempArrAddress(2)
                                Else
                                    Add1 = tempArrAddress(0)
                                    Add2 = tempArrAddress(1)
                                End If
                            ElseIf Request.Form("deliveryAddress") & "" <> "" Then
                                Add1 = ""
                                Add2 = Request.Form("deliveryAddress")
                        
                            Else
                                FromGoogleHighlight = ""
                                IsFromGoogle = false
                                Add1 = Address1Customer 'Request.Cookies("Address")
                                Add2 = Address2Customer 'Request.Cookies("Address2")
                                HouseNumber =  HouseNumberCustomer 'Request.Cookies("HouseNumber")
                              
                            End If
                            If Request.Form("deliveryPostCode") & "" <> "" Then
                                If InStr(Replace(PostaLCodeCustomer," ",""),Replace(Request.Form("deliveryPostCode")," ","")) > 0 AND Len(PostaLCodeCustomer) > Len(Request.Form("deliveryPostCode")) Then
                                    PostCode = PostaLCodeCustomer'Request.Cookies("PostCode")
                                Else
                                    PostCode = Request.Form("deliveryPostCode")
                                End If
                            ElseIf Request.Form("deliveryPC") & "" <> "" Then
                                PostCode = Request.Form("deliveryPC")
                            Else
                                PostCode = PostaLCodeCustomer 'Request.Cookies("PostCode")
                            End If
                        End If
                        
                        If IsFromGoogle OR 1 = 1 Then
                        '<label style="color:red">Your address was prefilled using Google Maps.  Please check it to ensure it is correct.</label>
                         %>
                        <% if checkout_Swap_address & "" <> "Yes" then %>
                                <div class="control-group col-sm-6 col-md-6" style="padding-left:0px;padding-right:0px">
                            <label class="control-label" for="House Number"><%=FilterData(arrData,"house_number_name") & " " %> *</label>                    
                            <div class="controls">
                                <input type="text" id="HouseNumber" name="HouseNumber"  class="form-control <%=FromGoogleHighlight %>" required title="<%=FilterData(arrData,"this_field_is_required")%>"  value="<%=HouseNumber %>" />                        
                                <% if 1 = 2 Then %><input type="checkbox" id="chkNoHouseNumber" name="chkNoHouseNumber" value="Y"/><label style="font-weight:normal;"><%=FilterData(arrData,"no_house_number_name") & " " %></label> <% end if %>
                            </div>
                            </div> 

                                <div style="padding-left:0px;padding-right:0px" class="control-group col-sm-6 col-md-6">
                            <label class="control-label" for="Address"><%=FilterData(arrData,"street_name") & " " %> *</label>
                            <div class="controls">
                      
                                <input type="text" id="Address" name="Address" class="form-control <%=FromGoogleHighlight %>" required title="<%=FilterData(arrData,"this_field_is_required")%>"  value="<%=Add1%>" />
                            </div>
                            </div> 
                    <%else %>
                             <div style="padding-left:0px;padding-right:0px" class="control-group col-sm-6 col-md-6">
                            <label class="control-label" for="Address"><%=FilterData(arrData,"street_name") & " " %> *</label>
                            <div class="controls">
                      
                                <input type="text" id="Address" name="Address" class="form-control <%=FromGoogleHighlight %>" required title="<%=FilterData(arrData,"this_field_is_required")%>"  value="<%=Add1%>" />
                            </div>
                            </div> 
                               <div class="control-group col-sm-6 col-md-6" style="padding-left:0px;padding-right:0px">
                            <label class="control-label" for="House Number"><%=FilterData(arrData,"house_number_name") & " " %> *</label>                    
                            <div class="controls">
                                <input type="text" id="HouseNumber" name="HouseNumber"  class="form-control <%=FromGoogleHighlight %>" required title="<%=FilterData(arrData,"this_field_is_required")%>"  value="<%=HouseNumber %>" />                        
                                <% if 1 = 2 Then %><input type="checkbox" id="chkNoHouseNumber" name="chkNoHouseNumber" value="Y"/><label style="font-weight:normal;"><%=FilterData(arrData,"no_house_number_name") & " " %></label> <% end if %>
                            </div>
                            </div> 
                    <% end if %>
                     <% Else %>   
                        <div style="padding-left:0px;padding-right:0px" class="control-group col-sm-12 col-md-12">
                    <label class="control-label" for="Address"><%=FilterData(arrData,"street_name") & " " %> *</label>
                    <div class="controls">
                      
                        <input type="text" id="Address" name="Address" class="form-control <%=FromGoogleHighlight %>" required title="<%=FilterData(arrData,"this_field_is_required")%>"  value="<%=Add1%>" />
                    </div>
                    </div> 
                        
                    <% End If
                     
                          %>
                  
                 <div class="control-group col-sm-12 col-md-12" style="padding-left:0px;padding-right:0px;">
                    <label class="control-label" for="Address2"><%=FilterData(arrData,"town_city") & " " %> *</label>
                    <div class="controls">
                        <input type="text" id="Address2" name="Address2" class="form-control <%=FromGoogleHighlight %>" required title="<%=FilterData(arrData,"this_field_is_required")%>"  value="<%=Add2%>" />
                    </div>
                </div>
                    
                 <div class="control-group col-sm-12 col-md-12" style="padding-left:0px;padding-right:0px;">
                    <label class="control-label" for="Postcode"><%=FilterData(arrData,"postcode") & " " %> *</label>
                    <div class="controls">
                        <!-- <input type="text" id="Postcode" name="Postcode" required class="form-control <%=FromGoogleHighlight %>" value="<%=PostCode %>" <% If Request.Form("deliveryType") = "d" AND Request.Form("deliveryPC") & "" <> "" then %>  readonly="true" <% end if %>  />-->
						<input type="text" id="Postcode" name="Postcode" required title="<%=FilterData(arrData,"this_field_is_required")%>"  class="form-control <%=FromGoogleHighlight %>" value="<%=PostCode %>" <% If Request.Form("deliveryType") = "d"  then %>  readonly="true" <% end if %>  />
                    </div>
                </div>

                    <% end if %>

                 <div class="control-group col-sm-12 col-md-12" style="padding-left:0px;padding-right:0px;">
                    <label class="control-label" for="Special"><%=FilterData(arrData,"special_instructions")%></label>
                    <div class="controls">
                        <textarea id="Special" name="Special" rows="4" class="form-control" ><%=Request.Form("Special")%></textarea>
                    </div>
                </div>
                    <% if isCheckCapcha  = "Yes" then %>
                    <div class="control-group col-sm-12 col-md-12" style="padding-left:0px;padding-right:0px;">
                    <iframe id="frmCapcha" height="101" width="100%" src="<%=SITE_URL %>iframeCapcha.html?v=3" scrolling="no" style="border:none;"></iframe>
                    </div>
                    <% end if %>
				   <div class="control-group col-sm-12 col-md-12" style="padding-left:0px;padding-right:0px;">              
                    <div class="controls">
                        <input type="checkbox" id="cookies" name="cookies" value="yes" checked><b> <%=FilterData(arrData,"remember_my_details_for_90_days")%></b>
                    </div>
                </div>
				<a href="javascript:window.history.back();" name="payment_type" value="nochex" class="btn btn-primary col-md-12" style="width: 180px; padding: 8px;margin-top:20px;"><%=FilterData(arrData,"back_to_menu")%> <span class="
                    glyphicon glyphicon-chevron-left" aria-hidden="true"></span></a>
            </fieldset>        
                          
		    </div>
        
		
           <%
               function CheckAddZero(byval t)
                    Dim result : result = ""
                        if cint(t) < 10 then
                            result = "0" & t
                        else
                            result = t
                        end if
                    CheckAddZero = result
                end function

                %>
			<div class="col-md-6">
                 <fieldset>
                <legend><%=FilterData(arrData,"your_order")%></legend>
                <b> <% If Request.Form("deliveryType") = "d" Then Response.Write(FilterData(arrData,"delivery")) Else Response.Write(FilterData(arrData,"collection")) End If %>
               (<%if Request.Form("asaporder") = "n" then%>  <%if Request.Form("deliveryType") = "c" then%>
				   <%=formatDateTimeC( DateAdd("n",vaveragecol,OrderDate))%>
				   <%else%>
				   ASAP
				   <%end if%><%else%>
                    <% ' Dim Duration : Duration = objRds("Duration") & "" 
                       Dim extraduration : extraduration  = ""
                        if Duration <> "" and Duration <> "0" then
                              '  Dim deliverytimeextra :  deliverytimeextra = formatDateTimeC(deliverytime)                                 
                                '    deliverytimeextra =  split(deliverytimeextra," ")(ubound(split(deliverytimeextra," ")) )                         
                                '    deliverytimeextra= cint(split(deliverytimeextra,":")(0)) * 60 + cint(split(deliverytimeextra,":")(1))
                                ''    deliverytimeextra = deliverytimeextra + cint(Duration)
                                '    deliverytimeextra =         CheckAddZero(((deliverytimeextra - (deliverytimeextra mod 60)) /60)) &":" & CheckAddZero(deliverytimeextra mod 60)
                                '    extraduration  = " - " & deliverytimeextra

                                extraduration =     Formattimeduration(formatDateTimeC(deliverytime),Duration)
                        end if
                     %>
                    <%= formatDateTimeC(deliverytime) %><%=extraduration %><%end if%>) </b><br /><br />
            <%
                
                'objCon.Open sConnString
                Set objRds = Server.CreateObject("ADODB.Recordset") 
                SQL = " select oi.*,mc.displayorder,  "
                 if cookieposition = 1 then
                    SQL = SQL & "mi.Name, mip.Name as PropertyName "
                else
                    SQL = SQL & "mi.Name"& cookieposition &" AS Name, mip.Name"& cookieposition &" AS PropertyName "
                end if
                SQL = SQL & ", (select top 1  ListDishesID from UpsellingGroup with(nolock) where CHARINDEX(',' + cast(oi.MenuItemId as varchar(8)) + ',', ',' +  ListDishesID + ',') > 0) as ListDishesID "
                SQL = SQL & "from ( OrderItems oi inner join MenuItems mi on oi.MenuItemId = mi.Id inner join menucategories   mc  with(nolock)  ON mc.id = mi.idmenucategory ) left join MenuItemProperties mip on oi.MenuItemPropertyId = mip.Id where oi.OrderId = " & vOrderId & " order by  mc.displayorder,oi.ID "
              
                objRds.Open SQL, objCon

                Dim listMenuItem : listMenuItem = ""
            if objRds.Eof then 
                Response.Write(FilterData(arrData,"no_item_in_your_order"))
                objRds.Close
                set objRds = nothing
                objCon.Close
                set objCon = nothing
                
            else %>

               
                    <table style="width: 100%" id="panel-item">  

                      <%
                          dim upsellinggroup : upsellinggroup = "" 
                    Do While NOT objRds.Eof  
                          
                          if listMenuItem = "" then
                                listMenuItem = objRds("MenuItemId") 
                          else
                                listMenuItem = listMenuItem & "," & objRds("MenuItemId")  
                          end if 
                          %>
                            <tr>
                                <td><%If objRds("Qta") > 1 Then %> 
                            (x <%= objRds("Qta") %>)
                        <% End If %> <%= objRds("Name") %>&nbsp;<%= objRds("PropertyName") %> 
						
						<%
                            if objRds("ListDishesID") & "" <> "" then
                                if upsellinggroup = "" then
                                   upsellinggroup = objRds("ListDishesID")
                                else
                                        upsellinggroup = upsellinggroup & "," & objRds("ListDishesID")
                                end if
                            end if
						'display toppings in basket area
						If objRds("dishpropertiesids") <> "" Then
						 
						        dishpropertiessplit=split(objRds("dishpropertiesids"),",")
					        for i=0 to ubound(dishpropertiessplit)					
					            dishpropertiessplit2=split(dishpropertiessplit(i),"|")
					            Set objRds_dishpropertiesprice = Server.CreateObject("ADODB.Recordset") 
                                SQL = "SELECT mn.ID, mn.dishpropertyprice, mng.dishpropertypricetype "
                                if cookieposition = 1 then
                                    SQL = SQL & ",mn.dishproperty,mng.dishpropertygroup "
                                else
                                    SQL = SQL & ",mn.dishproperty"& cookieposition &" AS dishproperty,mng.dishpropertygroup"& cookieposition &" AS dishpropertygroup "
                                end if
                                SQL = SQL & " FROM MenuDishproperties mn INNER JOIN MenuDishpropertiesGroups mng ON mn.dishpropertygroupid = mng.ID where (((mn.ID)=" & dishpropertiessplit2(1)  & ")) "
	                            objRds_dishpropertiesprice.Open SQL, objCon
					            if not objRds_dishpropertiesprice.EOF then
					                response.write "<BR> <small>" & objRds_dishpropertiesprice("dishpropertygroup") & ": " & objRds_dishpropertiesprice("dishproperty") & "</small>"
                                end if
					            objRds_dishpropertiesprice.close()
                                set objRds_dishpropertiesprice =  nothing
					        next
					    end if%>
						<%
						toppingtext=""
						If objRds("toppingids") <> "" Then 	
                            if cookieposition = 1 then
                                SQL = "SELECT distinct a.toppinggroupid,ap.toppingsgroup "
                            else
                                SQL = "SELECT distinct a.toppinggroupid,isnull(ap.toppingsgroup"& cookieposition &",'') AS toppingsgroup "
                            end if
                            SQL = SQL & " FROM MenuToppings a with(nolock)  "
                            SQL = SQL & "  join Menutoppingsgroups ap with(nolock) on a.toppinggroupid = ap.ID "
                            SQL = SQL & " where a.id in  (" & objRds("toppingids") & ") "
                            dim objRds_toppingids_group : Set objRds_toppingids_group = Server.CreateObject("ADODB.Recordset") 
                            objRds_toppingids_group.Open SQL, objCon
                            					
					        
                             Dim SQLTopping 
                             Dim toppinggroup : toppinggroup  =""
                            while not objRds_toppingids_group.EOF 
                                                toppingtext=""    
                                                toppinggroup = objRds_toppingids_group("toppingsgroup")
                                    Set objRds_toppingids = Server.CreateObject("ADODB.Recordset") 
                                    if cookieposition = 1 then
                                        SQLTopping = "SELECT m.topping,isnull(mp.toppingsgroup,'') as toppingsgroup  "
                                    else
                                        SQLTopping = "SELECT m.topping"& cookieposition &" as topping,isnull(mp.toppingsgroup"& cookieposition &",'') AS toppingsgroup "
                                    end if
                                    SQLTopping = SQLTopping &  " FROM MenuToppings m "
                                    SQLTopping =SQLTopping & "  left join Menutoppingsgroups mp on  m.toppinggroupid = mp.ID"
                                    SQLTopping =SQLTopping & "    where m.id in ("& objRds("toppingids") &")  and m.toppinggroupid= " & objRds_toppingids_group("toppinggroupid")
                          
                                    objRds_toppingids.Open SQLTopping, objCon
				                    Do While NOT objRds_toppingids.Eof 
						                    toppingtext = toppingtext & objRds_toppingids("topping") & ", "
                                           ' toppinggroup = objRds_toppingids("toppingsgroup")
						                    objRds_toppingids.MoveNext
						            loop
                                        objRds_toppingids.close()
                                    set objRds_toppingids = nothing
						            if toppingtext<>"" then
                                        if toppinggroup & "" = "" then
                                            toppinggroup = "Toppings"
                                          end if 
							            toppingtext=left(toppingtext,len(toppingtext)-2)
						                response.write "<small><br>"&toppinggroup&": " & toppingtext & "</small>"
						            end if
                            objRds_toppingids_group.movenext()
                            wend
                                objRds_toppingids_group.close()    
                                set objRds_toppingids_group = nothing
						 End If  %>
						</td>
                                <td style="padding-left: 20px; text-align: left;" valign="top"><%=CURRENCYSYMBOL%><%= formatcurentcyC(FormatNumber(objRds("Total"), 2)) %></td>                                    
                            </tr>
                    <%  
                        objRds.MoveNext        
                    Loop 
    
                            objRds.Close
                        set objRds = nothing
                          
                    %>
     
                         <tr>
                            <td style="padding-top: 5px">&nbsp;</td>
                            <td style="padding-top: 5px">&nbsp;</td>
                            <td style="padding-top: 5px">&nbsp;</td>
                        </tr>
						

							<%
        function CalculateSubtotalWithDiscount( byval orderID, byval discountvalue,byval VoucherMainType, byval ListID)
                            
        dim result : result = 0
       
        if ( VoucherMainType = "Dishes" or VoucherMainType ="Categories" )  then
                result = 0 
            dim SQL : SQL = "" 
                SQL = "select  MenuItemId,Total,IdMenuCategory from  OrderItems oi with(nolock)   " 
			    SQL= SQL & "  join MenuItems mi with(nolock) on oi.MenuItemId = mi.id "
			    SQL= SQL & " where oi.orderid  = " & orderID
             '   Response.Write(SQL & " ListID " & ListID  )
             '   Response.End
            
                dim RS_OrderTotal : set RS_OrderTotal = Server.CreateObject("ADODB.Recordset")
                RS_OrderTotal.Open SQL , objCon
                while not RS_OrderTotal.EOF
                    if VoucherMainType = "Dishes" then
        
                        if  instr("," & ListID,"," &  RS_OrderTotal("MenuItemId") & ",") > 0 then                            
                             result = result +  0.01 * cdbl(RS_OrderTotal("Total")) *  discountvalue    
                                        
                        end if
                    elseif VoucherMainType = "Categories" then
                         if  instr("," & ListID,"," & RS_OrderTotal("IdMenuCategory") & "," ) > 0 then
                             result = result + 0.01*  cdbl(RS_OrderTotal("Total")) *  discountvalue 
                               
                        end if
                    end if
                    RS_OrderTotal.movenext()
                wend
                   RS_OrderTotal.close()
                   set RS_OrderTotal = nothing   
        end if
      CalculateSubtotalWithDiscount =   result
    end function 
    dim DicountValueUsed : DicountValueUsed = 0
                                dim discountValueDisCat : discountValueDisCat = -1
                                if discountcodeused <>"" then         
                                                                
                                         Dim objRdsV,ListIncludeID,IncludeDishes_Categories
                                        Set objRdsV = Server.CreateObject("ADODB.Recordset") 
                                            objRdsV.Open "SELECT ListID,IncludeDishes_Categories FROM vouchercodes  with(nolock)  where IdBusinessDetail=" & vRestaurantId & " and vouchercode='" & vouchercode & "'", objCon, 1, 3 
                                        if not objRdsV.eof then
                                             ListIncludeID = objRdsV("ListID")
                                             IncludeDishes_Categories = objRdsV("IncludeDishes_Categories")
                                        end if
                                         if (IncludeDishes_Categories = "Dishes" or IncludeDishes_Categories = "Categories") and ListIncludeID & "" <> ""  then
                                                discountValueDisCat  = CalculateSubtotalWithDiscount(vOrderId,abs(Replace(discountcodeused,"%","")),IncludeDishes_Categories,ListIncludeID)                         
                                         end if
                                           if VoucherDiscontType = "Amount" then  
                                             discountValueDisCat = abs(Replace(discountcodeused,"%",""))
                                           end if
                                        objRdsV.close()
                                        set objRdsV = nothing                                  
                                    
                                %>
		                        <tr>
                                    <td style="padding-top: 5px; border-top: 1px dotted black;"><b><%=FilterData(arrData,"voucher") %></b><br /><%=vouchercode %><%if  VoucherDiscontType & "" <> "Amount" then%>(<%=discountcodeused%>)<%end if%> </td>
                                    <td style="padding-top: 5px; border-top: 1px dotted black;text-align: left;padding-left: 20px;">
			                         <% if VoucherDiscontType & "" = "Amount" then   
                                         DicountValueUsed = Cdbl(Replace(Replace(Replace(discountcodeused,"-",""),"%","")," ",""))
                                          %>
                                            <span id="voucher">-<%=CURRENCYSYMBOL%><%=formatcurentcyC( FormatNumber(Cdbl(Replace(Replace(Replace(discountcodeused,"-",""),"%","")," ","")) ,2)) %> </span></td>
                                     <%else %>
			                                    <% 
                                                    if discountValueDisCat >= 0 then 
                                                        DicountValueUsed = discountValueDisCat
                                                     %>
			                                        <span id="voucher">-<%=CURRENCYSYMBOL%><%= formatcurentcyC(FormatNumber(discountValueDisCat,2)) %> </span></td>
                                                <%else 
                                                         DicountValueUsed = (( vOrderSubTotal * 100 )/(100- Cdbl(Replace(Replace(Replace(discountcodeused,"-",""),"%","")," ",""))) - vOrderSubTotal )
                                                    %>
                                                    <span id="voucher">-<%=CURRENCYSYMBOL%><%=formatcurentcyC( FormatNumber((( ( vOrderSubTotal + loyaltyamount ) * 100 )/(100- Cdbl(Replace(Replace(Replace(discountcodeused,"-",""),"%","")," ",""))) - ( vOrderSubTotal + loyaltyamount ) ),2)) %> </span></td>
                                                <%end if %>
                                    <%end if %>
                                    <td style="padding-top: 5px; border-top: 1px dotted black;">&nbsp;</td>
                                </tr>
		                        <%end if%>
                                
                              
        
                         <tr>
                            <td style="padding-top: 5px; border-top: 1px dotted black;"><%=FilterData(arrData,"subtotal") %></td>
                            <td style="padding-top: 5px; padding-left: 20px; text-align: left; border-top: 1px dotted black;"><%=CURRENCYSYMBOL%><%= formatcurentcyC(FormatNumber(vOrderSubTotal, 2))  %>
                                <input type="hidden" id="subtotal" value="<%=vOrderSubTotal %>"/>

                            </td>
                            <td style="padding-top: 5px; border-top: 1px dotted black;">&nbsp;</td>

                        </tr>       
                          <%
                                    dim amountcanredeem : amountcanredeem = 0   
                                    if  enableloyalty = "Yes" and number_get_point > 0 then
                                             earnpointthisorder =  Round(( vOrderSubTotal  -  DicountValueUsed  ) / number_get_point)                                  
                                    end if
                                    
                                    
                                    if enableloyalty = "Yes" and CollectedPoint >-1 then
                                        amountcanredeem =  Math.floor( CollectedPoint / point_get_money )
                                    
                                        
                                            
                         %>
                                  <tr>
                                        <td style="padding-top: 5px; border-top: 1px dotted black;"><%=FilterData(arrData,"points_collected") %></td>
                                        <td style="padding-top: 5px; padding-left: 20px; text-align: left; border-top: 1px dotted black;"><%=CollectedPoint %>                                          
                                        </td>
                                        <td style="padding-top: 5px; border-top: 1px dotted black;">&nbsp;</td>

                                    </tr>    
                                   
                                    <tr>
                                        <td style="padding-top: 5px; border-top: 1px dotted black;">
                                            
                                            <%=FilterData(arrData,"points_redeem") %> (<%=CURRENCYSYMBOL%><%=amountcanredeem %>)

                                        </td>
                                        <td style="padding-top: 5px; padding-left: 20px; text-align: left; border-top: 1px dotted black;">                                          
                                             <% if loyaltyamount  = 0 and amountcanredeem > 0 and amountcanredeem >= cdbl(allow_amount_redeem) and cdbl(allow_amount_redeem) > 0  then %>    
                                                <span style="text-decoration:underline;color:blue;cursor:pointer;" id="redeemamount" onclick="Updateloyalty(<%=amountcanredeem %>);"><%=FilterData(arrData,"redeem") %> </span>
                                            <%end if %>
                                        </td>
                                        <td style="padding-top: 5px; border-top: 1px dotted black;">&nbsp;</td>

                                    </tr>    
                                    
                                    <tr>
                                         <% dim loyalty_icon : loyalty_icon =  FilterData(arrData,"loyalty_icon")  & ""
                                                loyalty_icon = replace( loyalty_icon,"x1",CURRENCYSYMBOL & number_get_point)
                                                loyalty_icon = replace( loyalty_icon,"x2",point_get_money)
                                                loyalty_icon = replace( loyalty_icon,"x3",CURRENCYSYMBOL & "1")
                                                loyalty_icon = replace( loyalty_icon,"x4",CURRENCYSYMBOL&allow_amount_redeem)
                                               
                                             
                                             %>   
                                        <td style="padding-top: 5px; border-top: 1px dotted black;"><%=FilterData(arrData,"points_this_Order") %> <i data-toggle="tooltip" title="<%=loyalty_icon %>" class="fa fa-info-circle" aria-hidden="true" style="cursor:pointer;"></i> </td>
                                        <td style="padding-top: 5px; padding-left: 20px; text-align: left; border-top: 1px dotted black;"><%=earnpointthisorder %>                                           
                                        </td>
                                        <td style="padding-top: 5px; border-top: 1px dotted black;">&nbsp;</td>
                                    </tr>    
                                    <% if loyaltyamount > 0 then %>
                                    <tr>
                                        <td style="padding-top: 5px; border-top: 1px dotted black;"><%=FilterData(arrData,"loyalty") %></td>
                                        <td style="padding-top: 5px; padding-left: 20px; text-align: left; border-top: 1px dotted black;">-<%=CURRENCYSYMBOL %><%=formatcurentcyC(FormatNumber(loyaltyamount, 2)) %>                                           
                                        </td>
                                        <td style="padding-top: 5px; border-top: 1px dotted black;">&nbsp;</td>

                                    </tr>   
                                    <%end if %>
                         <%
                                    end if  %>

                        <% if CDbl(vOrderShipTotal) > 0 Then %>
                        <tr>
                            <td style="padding-top: 5px;"><%=FilterData(arrData,"delivery_fee") %></td>
                            <td style="padding-top: 5px; padding-left: 20px; text-align: left;"><%=CURRENCYSYMBOL%><%= formatcurentcyC(FormatNumber(vOrderShipTotal, 2))  %></td>
                            <td style="padding-top: 5px;">&nbsp;</td>
                        </tr>       
                        <% End if 
                        If CDBl(serviceChargeAmount) > 0 Then
                            %>
                         <tr>
                            <td style="padding-top: 5px;"><%=FilterData(arrData,"service_charge") %></td>
                            <td style="padding-top: 5px; padding-left: 20px; text-align: left;"><%=CURRENCYSYMBOL%><%= formatcurentcyC(FormatNumber(serviceChargeAmount, 2))  %></td>
                            <td style="padding-top: 5px;">&nbsp;</td>
                        </tr>       
                        <% End If %>
                        <%
                            
                         If CDBl(extra_chargesAmount) > 0 Then
                            %>
                         <tr>
                            <td style="padding-top: 5px;"><%=FilterData(arrData,"extra_charges") %></td>
                            <td style="padding-top: 5px; padding-left: 20px; text-align: left;"><%=CURRENCYSYMBOL%><%= formatcurentcyC(FormatNumber(extra_chargesAmount, 2))  %></td>
                            <td style="padding-top: 5px;">&nbsp;</td>
                        </tr>       
                        <% End If %>
                        <% if cdbl(TaxAmount) > 0 then  %>
                            <tr>
                                <td style="padding-top: 5px;"><%=FilterData(arrData,"tax") %>(<%=Tax_Percent %>%)</td>
                                <td style="padding-top: 5px; padding-left: 20px; text-align: left;"><%=CURRENCYSYMBOL%><%= formatcurentcyC(FormatNumber(TaxAmount, 2) ) %></td>
                                <td style="padding-top: 5px;">&nbsp;</td>
                            </tr>   
                        <% end if %>
                        <% if cdbl(TipAmount) > 0 then  %>
                           
                             <% function WriteCheck(byval value1, byval value2)
                                    dim result : result = "" 
                                    if value1 & "" = value2 & ""  then
                                        result = "selected"
                                    end if
                                    WriteCheck = result
                                end function
                             '    Response.Write("Tip_Rate " & Tip_Rate & "<br/>")
                             %>
                            <tr>
                                <td style="padding-top: 5px;"><%=FilterData(arrData,"tip") %><select  id="tip_custom" style="display:none;margin-left:10px;width:80px;" onchange="ChangeTip(this);">
                                                                    <% 
                                                                         dim x
                                                                        for x = 1 to 25 
                                                                        if x mod 5 = 0 then
                                                                         %>
                                                                        <option <%=WriteCheck(x,Tip_Rate) %> value="<%=x %>" style="font-weight:bold"><%=x %>%</option>
                                                                        <% else %>
                                                                        <option <%=WriteCheck(x,Tip_Rate) %> value="<%=x %>"><%=x %>%</option>
                                                                        <% end if %>
                                                                     <%next %>       
                                                                   
                                                                    <option <%=WriteCheck("custom",Tip_Rate) %> value="custom"><%=FilterData(arrData,"custom") %></option>
                                                                 </select>
                                    <% if Tip_Rate = "custom" then %>
                                     <input type="text" id ="tip_value" value="<%=FormatNumber(TipAmount, 2) %>" style="display:none;width:50px;"/>
                                    <%else %>
                                    <input type="text" readonly="readonly" id ="tip_value" value="<%=FormatNumber(TipAmount, 2) %>" style="display:none;width:70px;"/>
                                    <% end if %>

                                    <span style="text-decoration:underline;color:blue;cursor:pointer;" id="tip_edit" onclick="edit();"><%=FilterData(arrData,"edit") %></span>
                                    <span style="text-decoration:underline;color:blue;cursor:pointer;display:none;" id="tip_update" onclick="UpdateTip();"><%=FilterData(arrData,"supdate") %></span></td>
                                <td style="padding-top: 5px; padding-left: 20px; text-align: left;" id="lbTipmount"><%=CURRENCYSYMBOL%><%= formatcurentcyC(FormatNumber(TipAmount, 2))  %></td>
                                <td style="padding-top: 5px;">&nbsp;</td>
                            </tr>   
                        <% end if %>
                        <tr>
                            <td style="padding-top: 5px;"><b><%=FilterData(arrData,"total") %></b></td>
                            <td style="padding-top: 5px; padding-left: 20px; text-align: left;"><%=CURRENCYSYMBOL%><b id="ordertotal"><%= formatcurentcyC(FormatNumber(vOrderTotal, 2))  %></b></td>
                            <td style="padding-top: 5px;">&nbsp;</td>
                        </tr>    
                         
                        <tr>
                            <td colspan="3">&nbsp;</td>    
                        </tr>
                        </table>
                     <style>
                         .product-line{display:table;width:100%;}
                         .product-line__content-left{
                                width: 60%;
                                margin-right: auto;
                                display: table-cell;                                
                                vertical-align: middle;
                         }
                         .product-line__content-right{
                             width: 40%;display: table-cell;text-align: right;vertical-align: middle;

                         }
                         .product-line__price{
                                margin-right: 25px;
                                margin-left: 20px;
                                display: inline-block;    
                                vertical-align: middle;
                         }
                     </style>
                     <% if ( signupsubscribe = "Yes" or signupsubscribe = "No")  and URL_Special_Offer <> "" and Showsubscribe =  "True" then %>
                        <table style="width: 100%">
                             
                            <tr>
                            <td colspan="3">

                             <div class="form-check">
                                 <% if signupsubscribe = "Yes"  then %>
                                    <input class="form-check-input" type="checkbox" value="1" name="signup">                                 
                                    <label class="form-check-label" for="flexCheckChecked"><%=FilterData(arrData,"s_signuptext") %></label>
                                 <% else %>                                                    
                                    <label class="form-check-label" for="flexCheckChecked"><a href="<%=URL_Special_Offer %>"><%=FilterData(arrData,"s_signuptext") %></a></label>
                                 <%end if %>
                             </div>
                            </td>    
                        </tr>
                       <tr>
                            <td colspan="3">&nbsp;</td>    
                        </tr>
                               
                        </table>    
                
                     <%end if %>  
                        <table style="width:100%">
                            <tr>
                            <td colspan="3">

                                <div id="divVoucherCode" style="padding:0px 0px 15px 0px;max-width:555px;">
                                     <button type="button" class="btn btn-xs btn-block" id="vouchercodeshow" style="background-color:#eeeeee;color:#7d7c7c;height:45px;margin-bottom:20px;"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> <%=FilterData(arrData,"add_voucher_code") %></button>
	                                <button type="button" class="btn  btn-xs btn-block" id="vouchercodehide"  style="display:none;background-color: #eeeeee;color:#7d7c7c  ;"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span> <%=FilterData(arrData,"sclose") %></button>	
                                    <div class="panel panel-default" style="display:none;" id="divVoucherCode1" >
                                        <div class="panel-body">           
						                                <div class="row">
                                  <div class="col-xs-7">
                                    <label class="sr-only" for="vouchercode">><%=FilterData(arrData,"enter_code") %></label>
                                    <input type="text" class="form-control noSubmit" id="vouchercode" name="vouchercode" placeholder="<%=FilterData(arrData,"enter_code") %>">
                                  </div> <div class="col-xs-3">
  
   
  
                                   <input  class="btn btn-default" type="button" onclick="VoucherCode();" value="<%=FilterData(arrData,"submit") %>"/>
                                 </div>
 
                                 <div class="col-xs-1">&nbsp;</div>
 
              
                                                    </div>
                                    </div>
                                    <div id="divVoucherCodeAlert" style="margin: 1px auto;text-align: center;color:red;"> </div>
                                 </div>
                                        </div>
                            </td>    
                        </tr>
                             <% if upsellinggroup & "" <> "" then 
                                dim sDayOfWeek
                                    sDayOfWeek = DatePart("w", DateAdd("h",houroffset,now), vbMonday, 1)
                                     function formatHourMin(byval hh, byval mm)
                                        dim result :  result = ""
                                        if hh < 10 then
                                            hh = "0" & hh
                                        end if
                                        if mm < 10 then
                                            mm = "0" & mm
                                        end if
                                        result = hh & ":" & mm
                                        formatHourMin = result
                                    end function
                                     Dim hhmm1 : hhmm1=  formatHourMin(Hour(DateAdd("h",houroffset,now)),Minute(DateAdd("h",houroffset,now)))
                          dim RS_ProductUpselling : set RS_ProductUpselling = Server.CreateObject("ADODB.Recordset")
                           SQLDetails = " select top 10 mi.id, mi.hidedish,Price,mi.Photo,Code, "
                            if cookieposition  = 1 then
                                SQLDetails = SQLDetails & "mi.Name "
                            else
                                SQLDetails = SQLDetails & "mi.Name"& cookieposition &" AS Name"
                            end if
                                SQLDetails = SQLDetails & " from MenuItems mi with(nolock) join  "
                                SQLDetails = SQLDetails & " ( menucategories   mc  with(nolock)    "
                                SQLDetails = SQLDetails & "        INNER JOIN Category_Openning_Time  ct  with(nolock)  "
                                SQLDetails = SQLDetails & "          on ( ct.categoryid = mc.id and ct.DayValue= " & sDayOfWeek & "   and ct.hour_from <= '" & hhmm1&"' and hour_to >= '"&hhmm1&"' and ct.status = 'ACTIVE'  )  ) "
                                SQLDetails = SQLDetails & "  ON mc.id = mi.idmenucategory "
                                SQLDetails = SQLDetails & " where  mi.hidedish <> 1 and   mi.id in (" & upsellinggroup & ")"
                                SQLDetails = SQLDetails & " and  mc.idbusinessdetail = " & vRestaurantId 
                                SQLDetails = SQLDetails & " and mi.idbusinessdetail = " & vRestaurantId 
                                if enable_stockstatus & "" = "1" then
                                    SQLDetails = SQLDetails & "        AND isnull(i_quantity,0) > 0 "
                                end if 
                                 if listMenuItem & "" <> ""  then
                                     SQLDetails = SQLDetails & " and  mi.id not in (" & listMenuItem & ")"   
                                 end if
                               SQLDetails = SQLDetails & " order by newid()"
                         
                            RS_ProductUpselling.Open SQLDetails, objCon
                                 dim menuprice
                            if not RS_ProductUpselling.EOF then
                                 %>
                                <tr style="height:10px;">
                                     <td colspan="3">
                                       
                                     </td>
                                 </tr>
                                 <tr>
                                     <td colspan="3" style="font-weight:bold;">
                                         <%=FilterData(arrData,"would_you_also_like") %>                                         
                                     </td>
                                 </tr>
                                 <%
                                     dim Photo 
                                 while not RS_ProductUpselling.EOF
                                    menuprice = RS_ProductUpselling("Price")
                                     Photo = RS_ProductUpselling("Photo") & ""
                                    %>
                                    <tr>
                                        <td colspan="3">
                                            <div class="product-line">                                           
                                                        <div class="product-line__content-left">
                                                            <div class="d-flex-center d-flex-start">
                                                                 <%
                                                                            dim styleMarginleft : styleMarginleft =""
								                                    If Photo <> "" Then 
                                                                           ' Response.Write("Photo " & Photo & "<br/>")
                                                                            styleMarginleft = "style='margin-left:12px;' "
								                                        'photo=1
                                                                     %>
                                                                            <div  class="product10w photo" data-toggle="modal" data-target="#lightbox">  
                                                                                <img data-src="<%=SITE_URL %>Images/<%=vRestaurantId %>/<%=Photo%>" class="img-rounded lazy" alt="<%= RS_ProductUpselling("Name")%>" style="vertical-align: top;width:30px;max-width:40px;" /> 
                                                                                    <div class="overlay">
                                                                                            <a href="javascript:;"  class="magnifying-glass-icon foobox" style="top:12px;left:20px;">
                                                                                            <i class="fa fa-search"></i>
                                                                                            </a>
                                                                                    </div>
						                                                    </div>	
                                                                    <div class="product-line__number" style="margin-left:12px;margin-right:12px;display:inline-block;"></div>
                                                                    <%End If %>
                                                                
                                                                <div class="product-line__description desc"><%= RS_ProductUpselling("Name") %></div>
                                                                </div>
                                                             
                                                        </div>
                                                        <div class="product-line__content-right" style="width:85%">
                                                            <div class="d-flex-center d-flex-end">  
                                                                        <div class="product-line__price"><b><%=CURRENCYSYMBOL%><%= formatcurentcyC(FormatNumber(menuprice, 2)) %></b></div>
                                                                      <div class="product-line__action-btn">	                                                                                            
                                                                         <div align="right">                                                                  
                                                                                <button class="btn btn-success btnadd" type="button" onclick="AdditemTocart('<%=RS_ProductUpselling("id")&""%>','' ,'' ,'' ,'1')">
                                                                                      <span class="glyphicon glyphicon-plus"></span>
                                                                                </button>    
                                                                        </div>
                                                                    </div>	
                                                             </div>				
                                                        </div>
                                             </div>
                                        </td>
                                    </tr>
                                    <%
                                    RS_ProductUpselling.movenext()
                                 wend
                            end if
                                 RS_ProductUpselling.close()
                             set RS_ProductUpselling = nothing 
                          %>  

                      <% end if %>


                            <% if enable_term_of_trade  = "1" then %>
                            <tr>
                                <td colspan="3">                               
                                    <legend><%=FirstCapitalize(FilterData(arrData,"payments")) %></legend>                                    
                                    <div class="form-check">
                                      <input class="form-check-input" type="checkbox" value="" id="termoftrade">
                                      <label class="form-check-label" for="flexCheckChecked">                                
                                        <%=FilterData(arrData,"i_accept_the") %> <a href="#" data-toggle="modal" data-target="#termoftrademodal"><%=FilterData(arrData,"term_of_trade") %></a>
                                      </label>
                                    </div>                                  
                                    <br />                              
                                </td>
                            </tr>
                            <%end if %>
                           
                        <tr>
                            <td colspan="3" style="text-align: center" >
                                <div id="processpayment" class="processpaymentblock"  style="max-width:555px;">
						<% 
                            if CREDITCARDSURCHARGE & "" = "" then
                                CREDITCARDSURCHARGE = 0
                            end if
                            Dim disableButton, popoverAttr, divCount
                            divCount = 0
                            disableButton = "disabled"
                             popoverAttr = "data-trigger=""hover"" data-toggle=""popover"" data-placement=""top"" data-content=""Minimum order " & CURRENCYSYMBOL & MinimumAmountForCardPayment & """"
                            If orderTotalAmount >= MinimumAmountForCardPayment Then 
                            
                                disableButton = ""
                                popoverAttr = ""
                             End If %>
						<% 
                             dim isOrder : isOrder =  false 
                            IF NOCHEX="Yes" THEN
                                isOrder =  true
                                divCount = divCount + 1%>
                             
                              <div class="block-direct" <%=popoverAttr %>>
							        <button  <%=disableButton %>   type="submit" name="payment_type" value="nochex"  class="btn btn-primary btn-block" ><%=FilterData(arrData,"pay_by_debit") %>
                                      
							        </button> 
                                   <% if cdbl( CREDITCARDSURCHARGE) > 0 then  %>
                                        <div>
                                         (<%=CURRENCYSYMBOL%><%=CREDITCARDSURCHARGE & " "%> <%=FilterData(arrData,"surcharge") %>)
                                        </div>
                                       <% end if %>
                            </div>
					    <%end if%>		
					<% IF PAYPAL="Yes" THEN
                        divCount = divCount + 1 
                         if isOrder = true then
                          %>
                          <div class="divider-or"><%=FilterData(arrData,"sor") %></div>
                          <%
                         end if
                               isOrder = true
                        %>
                                     <div class="block-direct" <%=popoverAttr %>>
                              <button <%=disableButton %>  type="submit" name="payment_type" value="paypal"  class="btn btn-primary btn-block btn-paypal">
								
                              </button>
                                  <% if cdbl( CREDITCARDSURCHARGE) > 0 then  %>
                                  <div>
                                  (<%=CURRENCYSYMBOL%><%=CREDITCARDSURCHARGE & " "%> <%=FilterData(arrData,"surcharge") %>)
                                   </div>
                                  <% end if %>
                            </div>
                           

                    <%end if%>	    

                    <% IF WORLDPAY="Yes" THEN
                        divCount = divCount + 1
                         if isOrder = true then
                        %>
                             <div class="divider-or"><%=FilterData(arrData,"sor") %></div>
                                    <%
                        end if
                                        isOrder =  true
                        %>
                           
                              <div class="block-direct" <%=popoverAttr %>>
                              <button <%=disableButton %>  type="submit" name="payment_type" value="worldpay"  class="btn btn-primary btn-block btn-worldpay" ><!--Pay by Debit/Credit Card (Worldpay)-->
                                
                              </button>
                                    <% if cdbl( CREDITCARDSURCHARGE) > 0 then  %>
                                  <div>
                                  (<%=CURRENCYSYMBOL%><%=CREDITCARDSURCHARGE & " "%> <%=FilterData(arrData,"surcharge") %>)
                                      </div>
                                  <% end if %>
                            </div>
					<%end if%>	
					
                                     <%

                                    if  ISSTRIPE = "Yes" then
                                        divCount = divCount + 1
                                        if isOrder = true then
                                        %>
                                           <div class="divider-or"><%=FilterData(arrData,"sor") %></div>               
                                        <%
                                        end if
                                             %>                                               
                                                <div class="block-direct" <%=popoverAttr %>>
                                                  <button <%=disableButton %>  type="submit" name="payment_type" value="stripe"  class="btn btn-primary btn-block btn-stripe" >
                                                    
                                                  </button>
                                                    <% if cdbl( CREDITCARDSURCHARGE) > 0 then  %>
                                                        <div>
                                                        (<%=CURRENCYSYMBOL%><%=CREDITCARDSURCHARGE & " "%> <%=FilterData(arrData,"surcharge") %>)
                                                            </div>
                                                    <% end if %>
                                                </div>

                                             <%   
                                                 isOrder  = true
                                    end if

                                       if disableButton = "" and enable_StripePaymentButton = "Yes" then  
                                                  divCount = divCount + 1
                                                  if isOrder = true then  
                                                %>
                                                 <div class="divider-or" id="stripeor" style="display:none;"><%=FilterData(arrData,"sor") %></div>   
                                                <% end if %>
                                            	<div class="block-direct" <%=popoverAttr %>>                                   
                                    <!-- #include file="Payments/stripe/stripepayment.asp" -->
                                                <% if cdbl( CREDITCARDSURCHARGE) > 0 then  %>
                                                <div id="idsurchage" style="display:none;color:grey;">(<%=CURRENCYSYMBOL%><%=CREDITCARDSURCHARGE& " "%> <%=FilterData(arrData,"surcharge") %>)</div>
                                                <%end if %>
                                               </div>
                                          <%
                                              isOrder  = true
                                              end if %>
                                    
									<%

                                    if  ISMPESA = "Yes" then
                                        divCount = divCount + 1
                                        if isOrder = true then
                                        %>
                                           <div class="divider-or"><%=FilterData(arrData,"sor") %></div>               
                                        <%
                                        end if
                                             %>                                               
                                                <div class="block-direct" <%=popoverAttr %>>												  
                                                  <button <%=disableButton %>  type="submit" name="payment_type" value="mpesa"  class="btn btn-primary btn-block btn-mpesa">
                                                    <!--<img src="<%=SITE_URL %>img/mpesa_btn.png" style="height: 40px;"/> <br>Mpesa-->
                                                  </button>
                                                      <% if cdbl( CREDITCARDSURCHARGE) > 0 then  %>
                                                     (<%=CURRENCYSYMBOL%><%=CREDITCARDSURCHARGE & " "%> <%=FilterData(arrData,"surcharge") %>)
                                                      <% end if %>
                                                </div>

                                             <%   
                                                 isOrder  = true
                                    end if
                                            
                                    %>
									
									<%									
                                    if  ISEKASHU = "Yes" then
                                        divCount = divCount + 1
                                        if isOrder = true then
                                        %>
                                           <div class="divider-or"><%=FilterData(arrData,"sor") %></div>               
                                        <%
                                        end if
                                             %>                                               
                                                <div class="block-direct" <%=popoverAttr %>>												  
                                                  <button <%=disableButton %>  type="submit" name="payment_type" value="ekashu"  class="btn btn-primary btn-block btn-ekashu">
                                                    <!--<img src="<%=SITE_URL %>img/ekashu_btn.png" style="height: 40px;"/> <br>Ekashu-->
                                                  </button>
                                                     <% if cdbl( CREDITCARDSURCHARGE) > 0 then  %>
                                                       (<%=CURRENCYSYMBOL%><%=CREDITCARDSURCHARGE & " "%> <%=FilterData(arrData,"surcharge") %>)
                                                      <%end if %>
                                                </div>

                                             <%   
                                                 isOrder  = true
                                    end if
                                            
                                    %>
									
									<%

                                    if  ISONECLICK = "Yes" then
                                        divCount = divCount + 1
                                        if isOrder = true then
                                        %>
                                           <div class="divider-or"><%=FilterData(arrData,"sor") %></div>               
                                        <%
                                        end if
                                             %>                                               
                                                <div class="block-direct" <%=popoverAttr %>>												  
                                                  <button <%=disableButton %>  type="submit" name="payment_type" value="Truevo"  class="btn btn-primary btn-block btn-truevo">
                                                   <!--<img src="<%=SITE_URL %>img/truevo_btn.png" style="height: 40px;"/> <br>Truevo-->
                                                  </button>
                                                      <% if cdbl( CREDITCARDSURCHARGE) > 0 then  %>
                                                       (<%=CURRENCYSYMBOL%><%=CREDITCARDSURCHARGE & " "%> <%=FilterData(arrData,"surcharge") %>)
                                                     <% end if %>
                                                </div>

                                             <%   
                                                 isOrder  = true
                                    end if
                                    if onpay_enable = "Yes" then
                                              divCount = divCount + 1   
                                                  if isOrder = true then  
                                              %>
                                                     <div class="divider-or"><%=FilterData(arrData,"sor") %></div> 
                                              <%
                                                  end if       
                                    %>
                                     <div class="block-direct" <%=popoverAttr %>>												 
                                        <button <%=disableButton %>  type="submit" name="payment_type" value="OnPay"  class="btn btn-primary btn-block btn-onpay">
                                          <!--<img src="<%=SITE_URL %>img/onpay_btn.png" style="height: 40px;"/> <br>OnPay-->
                                        </button>
                                           <% if cdbl( CREDITCARDSURCHARGE) > 0 then  %>
                                            (<%=CURRENCYSYMBOL%><%=CREDITCARDSURCHARGE & " "%> <%=FilterData(arrData,"surcharge") %>)
                                            <%end if %>
                                    </div>
                                    <%
                                         isOrder  = true
                                        end if
                                         %>
                                     <% 
                                
                                         if enable_CashPayment = "Yes" then  
                                           if isOrder = true then
                                            %>
                                                 <div class="divider-or"><%=FilterData(arrData,"sor") %></div>
                                                        <%
                                            end if %>
                                    <div class="block-cash">
							        <!--<button  type="submit" name="payment_type" value="cash delivery"  class="btn btn-info btn-block"><%=FilterData(arrData,"pay_by_cash") %></button>   -->
                                        <button type="submit" name="payment_type" value="cash delivery" class="btn btn-info btn-block">
                                           <img src="<%=SITE_URL %>Images/cash-in-hand.png" style="height: 40px;"/> <br><%=FilterData(arrData,"pay_by_cash") %>
                                        </button> 
                                    </div>
                                    <% end if %>
                                    </div>
                            </td>
                        </tr>

                    </table>

                      <script type="text/javascript">
                                    $('[data-toggle="tooltip"]').tooltip();
                                  function showtermoftrade(id)
                                  {
                            
                                  }
                                function IsInvalidTip(str)
                                {
                                    var patt = new RegExp(/^(\d*\.)?\d+$/);
                                    var res = patt.test(str);
                                    return res;
                                }
                                function ChangeTip(obj)
                                {
                                    var tipetype = $(obj).val();
                                    if(tipetype != "custom"){
                                        $("#tip_value").attr("readonly","true");
                                        var TipValue = parseFloat($("#subtotal").val()) * tipetype * 1.0 / 100;
                                            TipValue = parseFloat(TipValue).toFixed(2);
                                        $("#tip_value").val(TipValue); 
                                    }else
                                        $("#tip_value").removeAttr("readonly");
                                            
                                }
                                function Updateloyalty(amount)
                                {
                                    //$.ajax({url: "<%=SITE_URL%>redeem.asp?id_r=<%=vRestaurantId%>&oid=<%=vOrderId%>&amount=" + amount + "&r=" + Math.random() , success: function(result){                                       

                                    //    location.reload();
                                    //}});
                                    $("#panel-item").load("<%=SITE_URL%>redeem.asp?id_r=<%= vRestaurantId %>&o=<%=vOrderId%>&cp=<%=CollectedPoint%>&ep=&acr=<%=amountcanredeem%>&amount=" + amount+ "&r=" +  Math.random() );
                                   
                                    //$.ajax({url: "<%=SITE_URL%>applydiscount.asp?id_r=<%= vRestaurantId %>&op=vouchercode&vouchercode=" + $('#vouchercode').val() , success: function(result){
                                    //    $("#panel-item").htm(result);
                                    //}});
                                    return false;

                                }
                                function UpdateTip()
                                {
                                    if($("#tip_value").val()=="")
                                    {
                                        alert("<%=FilterData(arrData,"please_input_tip") %>");
                                        return false;
                                    } else if(!IsInvalidTip($("#tip_value").val()))
                                    {
                                        alert("<%=FilterData(arrData,"the_tip_must_be") %>");
                                        return false;
                                    }   
                                    $.ajax({url: "<%=SITE_URL%>UpdateTip.asp?id_r=<%=vRestaurantId%>&oid=<%=vOrderId%>&tipamount=" + $("#tip_value").val() + "&tr=" + $("#tip_custom").val() , success: function(result){
                                        $("#tip_custom").hide();
                                        $("#tip_value").hide();
                                        $("#tip_edit").show();
                                        $("#tip_update").hide();
                                        $("#ordertotal").html(result);
                                        $("[name=amount]").val(result);
                                        $("#lbTipmount").html("<%=CURRENCYSYMBOL%>" + parseFloat($("#tip_value").val()).toFixed(2));
                                        location.reload();
                                    }});
                                }
                                
                                function edit()
                                {
                                    $("#tip_custom").show();
                                    $("#tip_value").show();
                                    $("#tip_edit").hide();
                                    $("#tip_update").show();
                                }
                                $("#vouchercodeshow").click(function(){
                                    $("#divVoucherCode1").show();
                                    $("#vouchercodeshow").hide();
                                    $("#vouchercodehide").show();
                                });

                                $("#vouchercodehide").click(function(){
                                    $("#divVoucherCode1").hide();
                                    $("#vouchercodeshow").show();
                                    $("#vouchercodehide").hide();
                                });
                                $(function(){
                                    $("input.noSubmit").keypress(function(e){
                                        var k=e.keyCode || e.which;
                                        if(k==13){
                                            e.preventDefault();
                                        }
                                    });
                                });
                                function VoucherCode() {
                                    $("#panel-item").load("<%=SITE_URL%>applydiscount.asp?id_r=<%= vRestaurantId %>&o=<%=vOrderId%>&op=vouchercode&vouchercode=" + $('#vouchercode').val()+ "&cp=<%=CollectedPoint%>&ep=<%=earnpointthisorder%>&acr=<%=amountcanredeem%>");
                                   
                                 
                                    return false;
                                }
                                function AdditemTocart(mi,mip,toppingids,dishproperties,qta)
                                {
                                    $("#panel-item").load("<%=SITE_URL%>shoppingcartcheckoout.asp?o=<%=vOrderId%>&id_r=<%= vRestaurantId %>&lg=<%= cookielg %>&ot=online&op=add&mi=" +mi+"&mip="+mip+"&toppingids=" + toppingids + "&dishproperties=" + dishproperties + "&qta=" + qta + "&cp=<%=CollectedPoint%>&ep=<%=earnpointthisorder%>&acr=<%=amountcanredeem%>");
                                   
                                    return false;
                                }
                      </script>
                    
                     
                <%
                End If
                %>  
            </fieldset>
		    </div>	
		                
	    </div>

    </form>

</div>
    <div id="lightbox" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="margin-top: 20px;
    margin-left: 20px;">
    <div class="modal-dialog">
        <button type="button" class="close hidden" data-dismiss="modal" aria-hidden="true">x</button>
        <div class="modal-content">
            <div class="modal-body">
                <img src="" alt=""  />
            </div>
        </div>
    </div>
</div>

    <% if enable_term_of_trade = "1" then %>
    <div id="termoftrademodal" class="modal fade">
	  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;</button>
        <!--    <h3 style="color: red" id="msgTitle">
                <%=FilterData(arrData,"closed") %></h3>-->
        </div>
  
        <div class="modal-body">
           <%=term_of_trade %>
        </div>

        <div class="modal-footer">
            <a href="#" data-dismiss="modal" class="btn btn-primary"><%=FilterData(arrData,"ok") %></a>
        </div>
    </div></div></div>
    <% end if %>
 
   <%  objCon.Close
        set objCon = nothing %>

<script type="text/javascript">
    function CheckDistanceLatLng(firstResult) {

            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({"address":firstResult }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK && results[0]) {
                   
                  
                    var tempStreetNumber2 = '', tempRouteName2 = '', tempLocalcity2= '', tempPostalTown2='';
		              
                    for (i = 0; i < results[0].address_components.length; i++)
                    {
                        if (results[0].address_components[i].types[0] == "street_number") {
                            tempStreetNumber2 = results[0].address_components[i].short_name + ' ';
                        }
                        else if (results[0].address_components[i].types[0] == "route") {
                            tempRouteName2 = results[0].address_components[i].short_name;
                        }
                        else if (results[0].address_components[i].types[0] == "locality") {
                            tempLocalcity2 = results[0].address_components[i].short_name;
                        }
                        else if (results[0].address_components[i].types[0] == "postal_town") {
                            tempPostalTown2 = results[0].address_components[i].short_name;
                        }
                    }
                     
                    $("#HouseNumber").val(tempStreetNumber2);

                    if (tempRouteName2 != "") $("#Address").val(tempRouteName2);
                    else if (tempLocalcity2 != "") $("#Address").val(tempLocalcity2);
                    else $("#Address").val("");

                    if(tempPostalTown2!="")
                        $("#Address2").val(tempPostalTown2);
                    else if (tempLocalcity2 != "") $("#Address2").val(tempLocalcity2);
                                  
                      
                    $("#frmMakeOrder").valid();
                }
                
            });  
            return ;          
    }
    <% if individualpostcodeschecking <> 0  AND ( UCase(Request.Form("isChangeExistingAddress")) & "" = "Y" OR Add1 = "" ) then %>
    CheckDistanceLatLng('<%=PostCode%>');
    <% end if %>
    $(document).ready(function () {
        if($("#processpayment button").length==4){
            if($("#processpayment").width() <=550){
                $("#paybycash").css("float","left");
                
            }else{
                $("#paybycash").css("float","none");
            }
        }
        $(window).on('resize', function () {
            if($("#processpayment button").length==4){
                if($("#processpayment").width() <=550){
                    $("#paybycash").css("float","left");
                    
                }else{
                    $("#paybycash").css("float","none");
                    
                }
            }
        }); 

        var hour = <%= DatePart("h", DateAdd("h",houroffset,now), vbMonday, 1) + 1%>;
        if(hour < 10) hour = '0' + hour;
        $("select[name=p_hour]").find('option[value=' + hour + ']').attr("selected", true);

       

        //        $.validator.addMethod(
        //            "zipcode",
        //            function (value, element) {
        //                alert('');
        //                var url = 'http://maps.googleapis.com/maps/api/distancematrix/json?origins=<%=sPostalCode %>&destinations=';
        //                var distance = -1;
        //                $.ajax({
        //                    type: 'GET',
        //                    url: url + value + "&mode=auto&language=en&sensor=false",
        //                    dataType: 'json',
        //                    success: function (data) {
        //                        if (data.rows
        //                        && data.rows.length > 0) {
        //                            if (data.rows[0].elements
        //                            && data.rows[0].elements.length > 0) {
        //                                if (data.rows[0].elements[0].status == 'OK')
        //                                    distance = data.rows[0].elements[0].distance.value;
        //                            }
        //                        }
        //                    },
        //                    data: {},
        //                    async: false
        //                });

        //                if (distance >= 0)
        //                    $("#OrderMinModal div.modal-body").html("Sorry, the maximum distance for delivery is " + (max_km / 1000).toFixed(2) + " Km <br />");

        //                return distance >= 0;
        //            },
        //            "ZipCode Not Correct"
        //        );

        jQuery.validator.setDefaults({
            errorPlacement: function (error, element) {
                // if the input has a prepend or append element, put the validation msg after the parent div
                if (element.parent().hasClass('input-prepend') || element.parent().hasClass('input-append')) {
                    error.insertAfter(element.parent());
                    // else just place the validation message immediatly after the input
                } else {
                    error.insertAfter(element);
                }
            },
            errorElement: "small", // contain the error msg in a small tag
            wrapper: "div", // wrap the error message and small tag in a div
            highlight: function (element) {
                $(element).closest('.control-group').addClass('error'); // add the Bootstrap error class to the control group
            },
            success: function (element) {
                $(element).closest('.control-group').removeClass('error'); // remove the Boostrap error class from the control group
            }
        });

        $("#frmMakeOrder").removeAttr("novalidate");
      

        // $("form").validate();
                $("#frmMakeOrder").validate({
                    rules: {
                        Email: {
                            required: true,
                            email: true
                        }
                    }
                });
        

        var isFormSubmitted = false;
        $("#frmMakeOrder").submit(function() {    
            $("#frmMakeOrder").validate();
            var isCheckCapcha = true;
                if($("#frmCapcha").length > 0 )
                    {
                            var x = document.getElementById("frmCapcha");
                            var y = (x.contentWindow || x.contentDocument);
                           var className =  $(y.document.getElementById("txtcapcha")).attr("class");
                        if(className == "jCaptcha valid error"){
                                  $(y.document.getElementById("txtcapcha")).focus();  
                             return false;
                           }
                           
                    }
             if($("#termoftrade").length > 0 &&  !$("#termoftrade").is(":checked"))
                {
                alert("<%=FilterData(arrData,"term_of_trade_jsalert")%>");
                $("#termoftrade").focus();
                return false;

                }
            if($("form").valid() ){
                if(isFormSubmitted) return false;  
                if($.trim( $("#hint").html()) !="") 
                {   $("#Email").focus();
                    return false;
    }
        //signup
            if($("[name=signupform]").is(":checked"))
                $("[name=signup]").val("Y");
                return true;
            }
           
        });
        $('[data-toggle="popover"]').popover({ trigger: "hover" });   

    });
	
    function checkShowButtonStripe()
    {
        $("#frmMakeOrder").validate();
        var isCheckCapcha = true;
        if($("#frmCapcha").length > 0 )
        {
            var x = document.getElementById("frmCapcha");
            var y = (x.contentWindow || x.contentDocument);
            var className =  $(y.document.getElementById("txtcapcha")).attr("class");
            if(typeof className == "undefined" ||  className == "jCaptcha valid error"){
                //$(y.document.getElementById("txtcapcha")).focus();  
                return false;
            }
                           
        }
        if($("form").valid() ){
            $("#payment-request-button").show();
            if ($("#payment-request-button").length > 0 && $.trim($("#payment-request-button").html()) != "") {
                $("#idsurchage").show();
                $("#stripeor").show();
            }
                
        }else
        {
            $("#payment-request-button").hide();
            $("#idsurchage").hide();
            $("#stripeor").hide();
            
        }
    }
    $(function(){
        checkShowButtonStripe();
        $("#panel-left").find("input").bind("change",function(){
            checkShowButtonStripe();
            
        });
        var loadedElements = 0;
        $('.lazy').lazy({
            beforeLoad: function(element){
                console.log('image  is about to be loaded');
            },
            afterLoad: function(element) {
                loadedElements++;
 
                console.log('image  was '  + loadedElements+' loaded successfully');
            },
            onError: function(element) {
                loadedElements++;             
                console.log('image could not be ' +loadedElements+' loaded');
            },
            onFinishedAll: function() {
                console.log('finished loading  elements ' + loadedElements);
                console.log('lazy instance is about to be destroyed' + loadedElements)
            }
        });
    })

    var screenmode = "deskstop";
    function scrollToV2(id)
    {
        console.log("ID " + id);
        // Scroll
        $('html,body').animate({scrollTop: $("#"+id).offset().top-170},'slow');
    }
    function detechScreen()
    {
        if($(window).width() <=992 && screenmode=="deskstop"){              
            screenmode= "mobile";            
        }else if($(window).width() > 992 && screenmode=="mobile"){           
            screenmode= "deskstop";
        }
    }
    detechScreen();
</script>
    
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.g